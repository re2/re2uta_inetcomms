/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <re2uta/WalkingHandler.h>
#include <re2uta/ReducedMsgController.h>

#include <re2uta/walking/StepGenerator.hpp>
#include <re2uta/walking/WalkPatternGenerator.hpp>
#include <re2/visutils/marker_util.h>
#include <eigen_conversions/eigen_msg.h>
#include <tf_conversions/tf_eigen.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <ros/ros.h>
#include <boost/foreach.hpp>
#include <string>
#include <iostream>

#include <re2uta_inetComms/WalkingInfo.h>
#include <std_msgs/String.h>
#include <std_msgs/Empty.h>
#include <visualization_msgs/MarkerArray.h>
#include <re2uta_inetComms/QueueInfo.h>

using namespace re2uta;

static const std::string WC_BDI_STR(           "Bdi");
static const std::string WC_RE2UTA_STR(        "Re2Uta");
static const std::string WC_OFF_STR(           "off");
static const std::string WC_BAL_STR(           "Balance");
static const std::string WC_CAPTPT_STR(        "CapturePoint");
static const std::string WC_CRAWL_STR(         "Crawl");
static const std::string SG_CIRCLE_STR(    "Circle");
static const std::string SG_NAO_STR(       "Nao");
static const std::string SG_MANUAL_STR(    "Manual");
static const std::string JC_NEURALNETWORK_STR( "NeuralNetwork");
static const std::string JC_PID_STR(           "Pid");
static const std::string FT_LEFT_STR(      "Left");
static const std::string FT_RIGHT_STR(     "Right");

/**
 * Constructor takes in a ros node and a pointer to the ReducedMsgController
 */
WalkingHandler::WalkingHandler( ros::NodeHandle node, ReducedMsgController *rmc )
{
    m_node = node;
    m_rmc = rmc;

    setup();
}

WalkingHandler::~WalkingHandler()
{
}

/**
 * Initialize the handler.
 */
void WalkingHandler::setup()
{
    // Grab the params from the param server (set via the launch file)
    int tempHandlerId;
    m_node.param<int>(         "/re2uta/WalkingHandler_id",               tempHandlerId,      0                        );
    m_node.param<std::string>( "/re2uta/WalkingHandler_rosTopicUIReq",    m_rosTopicSubUIVal, "/ui/walkingHandlerReq"  );
    m_node.param<std::string>( "/re2uta/BLARG_WalkingHandler_rosTopicGoalData", m_rosTopicPubVal,   "/goal_robot_side"       );
    // Yes, I made the above key fail on purpose. No, I don't recall why, but it was important to do so.

    m_handlerId = tempHandlerId;
    m_currentWalkCommander   = WC_BDI_STR;
    m_currentStepGenerator   = SG_CIRCLE_STR;
    m_currentJointController = JC_NEURALNETWORK_STR;
    m_currentFoot            = FT_LEFT_STR;

    m_walkCommanderChanged = false;

    // Setup the publisher and subscriber
    m_uiQueuePathSub = m_node.subscribe( "/ui/walkPathQueue", 1, &WalkingHandler::queuePathCB, this );
    m_uiSub = m_node.subscribe( m_rosTopicSubUIVal, 1, &WalkingHandler::requestData, this );
    m_uiPub = m_node.advertise<geometry_msgs::PoseStamped>(m_rosTopicPubVal, 1, false);
//    m_msgQueueSub = m_node.subscribe("/ui/msgQueueAction", 1, &WalkingHandler::handleMsgQueueAction, this);

    m_msgQueuePub = m_node.advertise<re2uta_atlasCommander::WalkPlan>("/commander/walkto",1,false);

    m_walkCommanderSelPub     = m_node.advertise<std_msgs::String>("/commander/walk_commander_selection",  1,true);
    m_stepGeneratorSelPub     = m_node.advertise<std_msgs::String>("/commander/step_generator_selection",  1,true);
    m_jointControllerSelPub   = m_node.advertise<std_msgs::String>("/commander/joint_controller_selection",1,true);
    m_walkCommanderChangedSub = m_node.subscribe("/commander/walk_commander_changed",1,&WalkingHandler::walkCommanderChangedCB, this);

    lFootTrajPub = m_node.advertise<nav_msgs::Path>(            "/walkTrajectory/l_foot", 5, true);
    rFootTrajPub = m_node.advertise<nav_msgs::Path>(            "/walkTrajectory/r_foot", 5, true);
    lFootPosePub = m_node.advertise<geometry_msgs::PoseStamped>("/walkTrajectory/l_foot_pose", 5, true);
    rFootPosePub = m_node.advertise<geometry_msgs::PoseStamped>("/walkTrajectory/r_foot_pose", 5, true);
    comTrajPub   = m_node.advertise<nav_msgs::Path>(            "/walkTrajectory/com",    5, true);
    simMarkerPub = m_node.advertise<visualization_msgs::Marker>("/walkTrajectory/sim",    5, true);
    m_footstepMarkerPub = m_node.advertise<visualization_msgs::MarkerArray>( "/footstep_planner/footsteps_array", 1 );

    m_node.param<int>("/re2uta/OdomHandler_id", m_odomId, 29);
}


////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
// HELPER FUNCTIONS ////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////


/**
 *
 */
void WalkingHandler::toBaseOneTwentySeven(float baseTenNum, char* baseOneTwentySevenNumber)
{
    int baseTenDecimalShifted;
    int baseOneTwentySevenDigit;

    baseTenDecimalShifted = (int)((baseTenNum*(pow(10,WALKING_NUM_DIGITS_TO_KEEP)))+0.00003); // +0.000001 because computers are stupid

    for(int exponent=0; exponent<3; ++exponent) // Always have three digits per value
    {
        baseOneTwentySevenDigit = (( (int)( baseTenDecimalShifted/(pow(WALKING_BASE,exponent)) ) ) % WALKING_BASE);

        baseOneTwentySevenNumber[2-exponent] = (char)baseOneTwentySevenDigit;
    }
}

/**
 *
 */
float WalkingHandler::toBaseTen(char* baseOneTwentySevenNumber)
{
    float baseTenNumber = 0;
    int digitIndex = 0;
    int baseOneTwentySevenDigit;

    for(int exponent=2; exponent>=0; --exponent) // Always have three digits per value
    {
        baseOneTwentySevenDigit = (int)(baseOneTwentySevenNumber[digitIndex]);
        ++digitIndex;

        baseTenNumber += ((baseOneTwentySevenDigit ) * pow(WALKING_BASE,exponent))*pow(10, -1*WALKING_NUM_DIGITS_TO_KEEP);
    }

    return baseTenNumber;
}


////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
// UNPACKING THINGS ON ROBOT SIDE //////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////

/**
 * On the robot side, take params and make appropriate settings.
 */
void WalkingHandler::pack( char* params )
{
    ROS_INFO("[WalkingHandler] Received walking command with params:");

    int paramsIndex = 0;
    int numSteps;

    // Unpack params
    int tempVal;
    unsigned char paramsChar;
    paramsChar = params[paramsIndex];
    paramsIndex++;

    bool stuffGoneDoneChanged = false;
    std::string prevVal;

    // Step generator
    prevVal = m_currentStepGenerator;
    tempVal = paramsChar % 10;
    if(tempVal == 1)
    {
        m_currentStepGenerator = SG_CIRCLE_STR;
        paramsChar = paramsChar - 1;
    }
    else if(tempVal == 2)
    {
        m_currentStepGenerator = SG_NAO_STR;
        paramsChar = paramsChar - 2;
    }
    else if(tempVal == 3)
    {
        m_currentStepGenerator = SG_MANUAL_STR;
        paramsChar = paramsChar - 3;
    }
    paramsChar = paramsChar / 10;
    if(std::strcmp(m_currentStepGenerator.c_str(), prevVal.c_str())!=0)
    {
        stuffGoneDoneChanged = true;
    }


    // Walk Commander
    prevVal = m_currentWalkCommander;
    tempVal = paramsChar % 10;
    if(tempVal == 1)
    {
        m_currentWalkCommander = WC_BDI_STR;
        paramsChar = paramsChar - 1;
    }
    else if(tempVal == 2)
    {
        m_currentWalkCommander = WC_RE2UTA_STR;
        paramsChar = paramsChar - 2;
    }
    else if(tempVal == 3)
    {
        m_currentWalkCommander = WC_OFF_STR;
        paramsChar = paramsChar - 3;
    }
    else if(tempVal == 4)
    {
        m_currentWalkCommander = WC_BAL_STR;
        paramsChar = paramsChar - 4;
    }
    else if(tempVal == 5)
    {
        m_currentWalkCommander = WC_CAPTPT_STR;
        paramsChar = paramsChar - 5;
    }
    else if(tempVal == 6)
    {
        m_currentWalkCommander = WC_CRAWL_STR;
        paramsChar = paramsChar - 6;
    }
    paramsChar = paramsChar / 10;
    if(std::strcmp(m_currentWalkCommander.c_str(), prevVal.c_str())!=0)
    {
        stuffGoneDoneChanged = true;
    }


    // Foot
    prevVal = m_currentFoot;
    tempVal = paramsChar;
    if(tempVal == 1)
    {
        m_currentFoot = FT_LEFT_STR;
        m_poseMsgs.start_swing_foot = m_poseMsgs.LEFT_FOOT;
    }
    else
    {
        m_currentFoot = FT_RIGHT_STR;
        m_poseMsgs.start_swing_foot = m_poseMsgs.RIGHT_FOOT;
    }
    if(std::strcmp(m_currentFoot.c_str(), prevVal.c_str())!=0)
    {
        stuffGoneDoneChanged = true;
    }

    paramsChar = params[paramsIndex];
    paramsIndex++;


    // Joint controller
    prevVal = m_currentJointController;
    tempVal = paramsChar % 10;
    if(tempVal == 1)
    {
        m_currentJointController = JC_NEURALNETWORK_STR;
        paramsChar = paramsChar - 1;
    }
    else if(tempVal == 2)
    {
        m_currentJointController = JC_PID_STR;
        paramsChar = paramsChar - 2;
    }
    paramsChar = paramsChar / 10;
    if(std::strcmp(m_currentJointController.c_str(), prevVal.c_str())!=0)
    {
        stuffGoneDoneChanged = true;
    }


    // Number of steps
    numSteps = paramsChar % 10;
//    paramsChar = paramsChar / 10;


    // Change modes
    if(stuffGoneDoneChanged)
    {
        std_msgs::String walkCmdrSelMsg;
        m_walkCommanderChanged = false;
        walkCmdrSelMsg.data = m_currentWalkCommander;
        m_walkCommanderSelPub.publish(walkCmdrSelMsg);

        std_msgs::String stepGenSelMsg;
        stepGenSelMsg.data = m_currentStepGenerator;
        m_stepGeneratorSelPub.publish(stepGenSelMsg);

        std_msgs::String jointCtrlrSelMsg;
        jointCtrlrSelMsg.data = m_currentJointController;
        m_jointControllerSelPub.publish(jointCtrlrSelMsg);

        ROS_INFO("[WalkingHandler] Received Walk Commander value of %s",m_currentWalkCommander.c_str());
        ROS_INFO("[WalkingHandler] Received Step Generator value of %s",m_currentStepGenerator.c_str());
        ROS_INFO("[WalkingHandler] Received Joint Controller value of %s",m_currentJointController.c_str());
        ROS_INFO("[WalkingHandler] Received Num of Points value of %d",numSteps);
        ROS_INFO("[WalkingHandler] Received Foot value of %s",m_currentFoot.c_str());

        ros::Duration d(0.15);
        bool youHaveBeenWarned = false;
        while(!m_walkCommanderChanged && ros::ok())
        {
            if(!youHaveBeenWarned)
            {
                ROS_WARN("[WalkingHandler] Waiting to confirm walk commander changes...");
                youHaveBeenWarned = true;
            }
            ros::spinOnce();
            d.sleep();
        }
        ROS_INFO("[WalkingHandler] WalkCommander successfully changed to %s",m_currentWalkCommander.c_str());

        // Tell the odom handler to send an updated odom to the user.
        char spoofedData[1];
        spoofedData[0] = 3; // Kin odom
        m_rmc->getDataHandler(m_odomId)->pack(spoofedData);
    }
    else
    {
        // Tell the robot where to go
        if(std::strcmp(m_currentStepGenerator.c_str(),SG_MANUAL_STR.c_str())==0)
        {
            // Unpack steps
            for(int i=0; i<numSteps; ++i)
            {
                paramsIndex = createPoseFromBuf(params, paramsIndex);
            }
        }
        else
        { // Circle or NAO Step generators
            paramsIndex = createPoseFromBuf(params, paramsIndex);
        }

        publish();
    }
}

/**
 * Convert the info from the char array into a pose msg and
 * add it to the WalkPlan msg.
 */
int WalkingHandler::createPoseFromBuf(char *params, int paramsIndex)
{
    char baseOneTwentySevenNumber[3];
    float positionX    = 0.0;
    float positionY    = 0.0;
    float positionZ    = 0.0;
    float orientationX = 0.0;
    float orientationY = 0.0;
    float orientationZ = 0.0;
    float orientationW = 0.0;

    // POSE
    // x
    baseOneTwentySevenNumber[0] = params[paramsIndex]; // 0
    paramsIndex++;
    baseOneTwentySevenNumber[1] = params[paramsIndex]; // 1
    paramsIndex++;
    baseOneTwentySevenNumber[2] = params[paramsIndex]; // 2
    paramsIndex++;
    positionX = toBaseTen(baseOneTwentySevenNumber);

    // y
    baseOneTwentySevenNumber[0] = params[paramsIndex]; // 3
    paramsIndex++;
    baseOneTwentySevenNumber[1] = params[paramsIndex]; // 4
    paramsIndex++;
    baseOneTwentySevenNumber[2] = params[paramsIndex]; // 5
    paramsIndex++;
    positionY = toBaseTen(baseOneTwentySevenNumber);

    // z
    //    baseOneTwentySevenNumber[0] = params[paramsIndex]; // 6
    //    paramsIndex++;
    //    baseOneTwentySevenNumber[1] = params[paramsIndex]; // 7
    //    paramsIndex++;
    //    baseOneTwentySevenNumber[2] = params[paramsIndex]; // 8
    //    paramsIndex++;
    //    positionZ = toBaseTen(baseOneTwentySevenNumber);

    // ORIENTATION
    // x
    //    baseOneTwentySevenNumber[0] = params[paramsIndex]; // 9
    //    paramsIndex++;
    //    baseOneTwentySevenNumber[1] = params[paramsIndex]; // 10
    //    paramsIndex++;
    //    baseOneTwentySevenNumber[2] = params[paramsIndex]; // 11
    //    paramsIndex++;
    //    orientationX = toBaseTen(baseOneTwentySevenNumber);

    // y
    //    baseOneTwentySevenNumber[0] = params[paramsIndex]; // 12
    //    paramsIndex++;
    //    baseOneTwentySevenNumber[1] = params[paramsIndex]; // 13
    //    paramsIndex++;
    //    baseOneTwentySevenNumber[2] = params[paramsIndex]; // 14
    //    paramsIndex++;
    //    orientationY = toBaseTen(baseOneTwentySevenNumber);

    // z
    baseOneTwentySevenNumber[0] = params[paramsIndex]; // 15
    paramsIndex++;
    baseOneTwentySevenNumber[1] = params[paramsIndex]; // 16
    paramsIndex++;
    baseOneTwentySevenNumber[2] = params[paramsIndex]; // 17
    paramsIndex++;
    orientationZ = toBaseTen(baseOneTwentySevenNumber);

    //w
    baseOneTwentySevenNumber[0] = params[paramsIndex]; // 18
    paramsIndex++;
    baseOneTwentySevenNumber[1] = params[paramsIndex]; // 19
    paramsIndex++;
    baseOneTwentySevenNumber[2] = params[paramsIndex]; // 20
    paramsIndex++;
    orientationW = toBaseTen(baseOneTwentySevenNumber);


    ROS_INFO("[WalkingHandler]   Pose:");
    ROS_INFO("[WalkingHandler]     x: %f", positionX);
    ROS_INFO("[WalkingHandler]     y: %f", positionY);
    ROS_INFO("[WalkingHandler]     z: %f", positionZ);
    ROS_INFO("[WalkingHandler]   Orientation:");
    ROS_INFO("[WalkingHandler]     x: %f", orientationX);
    ROS_INFO("[WalkingHandler]     y: %f", orientationY);
    ROS_INFO("[WalkingHandler]     z: %f", orientationZ);
    ROS_INFO("[WalkingHandler]     w: %f", orientationW);

    m_latestPoseMsg.pose.position.x    = positionX;
    m_latestPoseMsg.pose.position.y    = positionY;
    m_latestPoseMsg.pose.position.z    = positionZ;
    m_latestPoseMsg.pose.orientation.x = orientationX;
    m_latestPoseMsg.pose.orientation.y = orientationY;
    m_latestPoseMsg.pose.orientation.z = orientationZ;
    m_latestPoseMsg.pose.orientation.w = orientationW;
    m_latestPoseMsg.header.frame_id = "/odom";

    geometry_msgs::Pose currentPoseMsg;
    currentPoseMsg.position.x    = positionX;
    currentPoseMsg.position.y    = positionY;
    currentPoseMsg.position.z    = positionZ;
    currentPoseMsg.orientation.x = orientationX;
    currentPoseMsg.orientation.y = orientationY;
    currentPoseMsg.orientation.z = orientationZ;
    currentPoseMsg.orientation.w = orientationW;

    m_poseMsgs.goal_poses.push_back(currentPoseMsg);

    return paramsIndex;
}

void WalkingHandler::walkCommanderChangedCB(const std_msgs::EmptyConstPtr & msg)
{
    m_walkCommanderChanged = true;
}

/**
 * Publish the desired pose(s) to get the robot to walk.
 */
void WalkingHandler::publish()
{
    m_latestPoseMsg.header.frame_id = "/odom";
    m_uiPub.publish(m_latestPoseMsg);

    m_poseMsgs.header.frame_id = "/odom";
    m_msgQueuePub.publish(m_poseMsgs);
    m_poseMsgs.goal_poses.clear();
}




////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
// PACKING THINGS ON UI SIDE ///////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////

/**
 * Received the go ahead from the ui to queue or send the current walking info
 */
void WalkingHandler::queuePathCB( const re2uta_inetComms::WalkingInfoConstPtr & msg )
{
    bool changeSettingsOnly;
    bool queueMsg;

    changeSettingsOnly       = msg->changeSettingsOnly;
    queueMsg                 = msg->queueMsg;
    m_currentWalkCommander   = msg->walkCommander;
    m_currentStepGenerator   = msg->stepGenerator;
    m_currentJointController = msg->jointController;
    m_currentFoot            = msg->footChoice;

    if(!changeSettingsOnly)
    {
        sendToRobot(queueMsg);
    }
}

/**
 * From the ocu side, pack up the params and path, then send to robot
 */
void WalkingHandler::sendToRobot(bool queue)
{
    char baseOneTwentySevenNumber[3];

    ROS_INFO("[WalkingHandler] Received request for walking to a destination from UI. Sending req to RMC." );

    // Do something special for manual step generator
    if(std::strcmp(m_currentStepGenerator.c_str(),SG_MANUAL_STR.c_str())==0)
    {
        if(queue)
        { // If queue, queue entire queue of steps
            int dataOutSize = (m_queuedMsgs.size()*((2*3) + (2*3))) + 2; // +2 = params
            char* dataOut = new char[dataOutSize];
            int bufIndex = 0;
            float endX;
            float endY;

            ROS_INFO("[WalkingHandler] BufIndex: %d",bufIndex);
            bufIndex = setParams(dataOut);
            ROS_INFO("[WalkingHandler] BufIndex: %d",bufIndex);

            // Loop through m_queuedMsgs adding each to the buffer
            for(int i=0; i<m_queuedMsgs.size(); ++i)
            {
                bufIndex = addPose(dataOut, bufIndex, m_queuedMsgs.at(i));
                ROS_INFO("[WalkingHandler] BufIndex: %d",bufIndex);
                endX = m_queuedMsgs.at(i).pose.position.x;
                endY = m_queuedMsgs.at(i).pose.position.y;
                ROS_INFO_STREAM("[WalkingHandler] Packed footstep at x:"<<endX<<" y:"<<endY);
            }
            m_queuedMsgs.clear();


//            while(!m_queuedMsgs.empty())
//            {
//                bufIndex = addPose(dataOut, bufIndex, m_queuedMsgs.back());
//                endX = m_queuedMsgs.back().pose.position.x;
//                endY = m_queuedMsgs.back().pose.position.y;
//                ROS_INFO_STREAM("[WalkingHandler] Packed footstep at x:"<<endX<<" y:"<<endY);
//                m_queuedMsgs.pop_back();
//            }

            // Make up a description
            std::stringstream description;
            description << "Steps (";

            if(std::strcmp(m_currentFoot.c_str(),FT_LEFT_STR.c_str())==0)
            {
                description << " L";
            }
            else
            {
                description << " R";
            }

            description << ") ending at x:" << endX
                        << " y:"            << endY;

            m_rmc->queueMsg( m_handlerId, dataOut, dataOutSize, description.str() );
        }
        else
        { // Else if !queue, add step to queue of steps
            geometry_msgs::PoseStamped msgToBeQueued;

            msgToBeQueued.header = m_latestPoseMsg.header;
            msgToBeQueued.pose   = m_latestPoseMsg.pose;

            m_queuedMsgs.push_back(msgToBeQueued);
            makeStepPath(false);
        }
    }
    else
    { // Circle or NAO
        ROS_INFO("[WalkingHandler]   Pose:");
        ROS_INFO("[WalkingHandler]     x: %f", m_latestPoseMsg.pose.position.x);
        ROS_INFO("[WalkingHandler]     y: %f", m_latestPoseMsg.pose.position.y);
        ROS_INFO("[WalkingHandler]     z: %f", m_latestPoseMsg.pose.position.z);
        ROS_INFO("[WalkingHandler]   Orientation:");
        ROS_INFO("[WalkingHandler]     x: %f", m_latestPoseMsg.pose.orientation.x);
        ROS_INFO("[WalkingHandler]     y: %f", m_latestPoseMsg.pose.orientation.y);
        ROS_INFO("[WalkingHandler]     z: %f", m_latestPoseMsg.pose.orientation.z);
        ROS_INFO("[WalkingHandler]     w: %f", m_latestPoseMsg.pose.orientation.w);

        // Convert doubles to something smaller
        // Realistically, we can only walk a max of ~30m at a time due to laser constraints
        // Therefore, pose will be limited to +/- 30
        // z = 0

        // Orientation is always -1 <= 1 for z and w
        // What amount of accuracy do we require?
        // x = y = 0

        // Pose(2) * 3 chars each + orientation(2) * 3 chars each + 2 for params
        int dataOutSize = (2*3) + (2*3) + 2;
        char* dataOut = new char[dataOutSize];
        int bufIndex = 0;

        bufIndex = setParams(dataOut);
        bufIndex = addPose(dataOut,bufIndex, m_latestPoseMsg);

        if(queue)
        {
            std::stringstream description;

            if(std::strcmp(m_currentStepGenerator.c_str(),SG_CIRCLE_STR.c_str())==0)
            {
                description << "Circle path";
            }
            else if(std::strcmp(m_currentStepGenerator.c_str(),SG_NAO_STR.c_str())==0)
            {
                description << "NAU path";
            }

            description << " to x:" << m_latestPoseMsg.pose.position.x
                    << " y:"    << m_latestPoseMsg.pose.position.y;

            m_rmc->queueMsg( m_handlerId, dataOut, dataOutSize, description.str() );
        }
        else
        {
            m_rmc->send( m_handlerId, dataOut, dataOutSize );
        }
    } // End else (i.e. not manual step)
}

/**
 * Stuff the params into the buffer. Returns the buffer index
 */
int WalkingHandler::setParams(char *dataOut)
{
    int bufIndex = 0;

    // PARAMS
    unsigned char paramChar = 0;


    // Foot
    if(std::strcmp(m_currentFoot.c_str(),FT_LEFT_STR.c_str())==0)
    {
        paramChar = 1;
    }
    paramChar = paramChar * 10;


    // Walk commander
    if(std::strcmp(m_currentWalkCommander.c_str(),WC_BDI_STR.c_str())==0)
    {
        paramChar = paramChar + 1;
    }
    else if(std::strcmp(m_currentWalkCommander.c_str(),WC_RE2UTA_STR.c_str())==0)
    {
        paramChar = paramChar + 2;
    }
    else if(std::strcmp(m_currentWalkCommander.c_str(),WC_OFF_STR.c_str())==0)
    {
        paramChar = paramChar + 3;
    }
    else if(std::strcmp(m_currentWalkCommander.c_str(),WC_BAL_STR.c_str())==0)
    {
        paramChar = paramChar + 4;
    }
    else if(std::strcmp(m_currentWalkCommander.c_str(),WC_CAPTPT_STR.c_str())==0)
    {
        paramChar = paramChar + 5;
    }
    else if(std::strcmp(m_currentWalkCommander.c_str(),WC_CRAWL_STR.c_str())==0)
    {
        paramChar = paramChar + 6;
    }
    paramChar = paramChar * 10;


    // Step Gen
    if(std::strcmp(m_currentStepGenerator.c_str(),SG_CIRCLE_STR.c_str())==0)
    {
        paramChar = paramChar + 1;
    }
    else if(std::strcmp(m_currentStepGenerator.c_str(),SG_NAO_STR.c_str())==0)
    {
        paramChar = paramChar + 2;
    }
    else if(std::strcmp(m_currentStepGenerator.c_str(),SG_MANUAL_STR.c_str())==0)
    {
        paramChar = paramChar + 3;
    }
    // paramChar = paramChar * 10;

    dataOut[bufIndex] = paramChar;
    bufIndex++;
    paramChar = 0;

    // Number of steps
    paramChar = paramChar + m_queuedMsgs.size();
    paramChar = paramChar * 10;


//    if(queue)
//    {
//        paramChar = 1;
//    }
//    else
//    {
//        paramChar = 0;
//    }
//    paramChar = paramChar * 10;

    // Joint controller
    if(std::strcmp(m_currentJointController.c_str(),JC_NEURALNETWORK_STR.c_str())==0)
    {
        paramChar = paramChar + 1;
    }
    else if(std::strcmp(m_currentJointController.c_str(),JC_PID_STR.c_str())==0)
    {
        paramChar = paramChar + 2;
    }
    // paramChar = paramChar * 10;

    dataOut[bufIndex] = paramChar;
    bufIndex++;

    return bufIndex;
}

/**
 * Pack poseMsg into dataOut
 */
int WalkingHandler::addPose(char* dataOut, int bufIndex, geometry_msgs::PoseStamped poseMsg)
{
    char baseOneTwentySevenNumber[3];

    // POSE
    // x
    toBaseOneTwentySeven(poseMsg.pose.position.x, baseOneTwentySevenNumber);
    dataOut[bufIndex] = baseOneTwentySevenNumber[0];
    bufIndex++;
    dataOut[bufIndex] = baseOneTwentySevenNumber[1];
    bufIndex++;
    dataOut[bufIndex] = baseOneTwentySevenNumber[2];
    bufIndex++;

    // y
    toBaseOneTwentySeven(poseMsg.pose.position.y, baseOneTwentySevenNumber);
    dataOut[bufIndex] = baseOneTwentySevenNumber[0];
    bufIndex++;
    dataOut[bufIndex] = baseOneTwentySevenNumber[1];
    bufIndex++;
    dataOut[bufIndex] = baseOneTwentySevenNumber[2];
    bufIndex++;

    // z is always 0
    //    toBaseOneTwentySeven(poseMsg.pose.position.z, baseOneTwentySevenNumber);
    //    dataOut[bufIndex] = baseOneTwentySevenNumber[0];
    //    bufIndex++;
    //    dataOut[bufIndex] = baseOneTwentySevenNumber[1];
    //    bufIndex++;
    //    dataOut[bufIndex] = baseOneTwentySevenNumber[2];
    //    bufIndex++;

    // ORIENTATION
    // x is always 0
    //    toBaseOneTwentySeven(poseMsg.pose.orientation.x, baseOneTwentySevenNumber);
    //    dataOut[bufIndex] = baseOneTwentySevenNumber[0];
    //    bufIndex++;
    //    dataOut[bufIndex] = baseOneTwentySevenNumber[1];
    //    bufIndex++;
    //    dataOut[bufIndex] = baseOneTwentySevenNumber[2];
    //    bufIndex++;

    // y is always 0
    //    toBaseOneTwentySeven(poseMsg.pose.orientation.y, baseOneTwentySevenNumber);
    //    dataOut[bufIndex] = baseOneTwentySevenNumber[0];
    //    bufIndex++;
    //    dataOut[bufIndex] = baseOneTwentySevenNumber[1];
    //    bufIndex++;
    //    dataOut[bufIndex] = baseOneTwentySevenNumber[2];
    //    bufIndex++;

    // z
    toBaseOneTwentySeven(poseMsg.pose.orientation.z, baseOneTwentySevenNumber);
    dataOut[bufIndex] = baseOneTwentySevenNumber[0];
    bufIndex++;
    dataOut[bufIndex] = baseOneTwentySevenNumber[1];
    bufIndex++;
    dataOut[bufIndex] = baseOneTwentySevenNumber[2];
    bufIndex++;

    // w
    toBaseOneTwentySeven(poseMsg.pose.orientation.w, baseOneTwentySevenNumber);
    dataOut[bufIndex] = baseOneTwentySevenNumber[0];
    bufIndex++;
    dataOut[bufIndex] = baseOneTwentySevenNumber[1];
    bufIndex++;
    dataOut[bufIndex] = baseOneTwentySevenNumber[2];
    bufIndex++;

    return bufIndex;
}



////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
// NOT NEEDED //////////////////////////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////

// Not needed, but must implement
void WalkingHandler::unpack( unsigned char* data ){}
void WalkingHandler::convertSmallest(){}
void WalkingHandler::convertSmall(){}
void WalkingHandler::convertMedium(){}
void WalkingHandler::convertLarge(){}
void WalkingHandler::convertLargest(){}


////////////////////////////////////////////////////////
////////////////////////////////////////////////////////
// PATH CREATION FOR VISUALIZATION /////////////////////
////////////////////////////////////////////////////////
////////////////////////////////////////////////////////

/**
 * A 2D nav goal has been sent by the ui. Plan as best we can on the ocu side.
 */
void WalkingHandler::requestData( const geometry_msgs::PoseStampedConstPtr & msg )
{
    m_latestPoseMsg.header = msg->header;
    m_latestPoseMsg.pose   = msg->pose;

    tf::poseMsgToEigen( msg->pose, m_dest );

    if(std::strcmp(m_currentStepGenerator.c_str(),SG_CIRCLE_STR.c_str())==0)
    {
        makeCirclePath();
    }
    else if(std::strcmp(m_currentStepGenerator.c_str(),SG_NAO_STR.c_str())==0)
    {
        makeNaoPath();
    }
    else if(std::strcmp(m_currentStepGenerator.c_str(),SG_MANUAL_STR.c_str())==0)
    {
        makeStepPath(true);
    }
}

void WalkingHandler::makeCirclePath()
{
    ROS_INFO("[WalkingHandler] Making circle path");
    double destDist = 2;

    m_node.getParam( "dist" , destDist );

    nav_msgs::Path lFootTraj;
    nav_msgs::Path rFootTraj;
    nav_msgs::Path comTraj;

    lFootTraj.header.frame_id = "/odom";
    rFootTraj.header.frame_id = "/odom";
    comTraj.header.frame_id   = "/odom";

    geometry_msgs::PoseStamped poseStamped;
    poseStamped.header.frame_id = "/odom";

    Eigen::Affine3d src;
    tf::StampedTransform currentOdom;

    m_listener.lookupTransform("/odom", "/pelvis",
            ros::Time(0), currentOdom); // Time 0 = latest
    tf::TransformTFToEigen(currentOdom, src);
    src.translation().z() = 0;

    Eigen::Affine3d lCurrentPose = Eigen::Affine3d::Identity();
    Eigen::Affine3d rCurrentPose = Eigen::Affine3d::Identity();

    lCurrentPose = src;
    rCurrentPose = src;
    lCurrentPose.translation().z() = 0;
    rCurrentPose.translation().z() = 0;


    Eigen::Translation3d lTranslation( 0, -0.15, 0 );
    Eigen::Translation3d rTranslation( 0,  0.15, 0 );

    lCurrentPose = lCurrentPose * lTranslation;
    rCurrentPose = rCurrentPose * rTranslation;

    tf::poseEigenToMsg( lCurrentPose, poseStamped.pose );
    lFootTraj.poses.push_back( poseStamped );

    tf::poseEigenToMsg( rCurrentPose, poseStamped.pose );
    rFootTraj.poses.push_back( poseStamped );


    double halfStepSize    = 0.2;
    double halfStanceWidth = 0.15;
    Foot foot;

    if(std::strcmp(m_currentFoot.c_str(),FT_LEFT_STR.c_str())==0)
    {
        foot = LEFT_FOOT;
    }
    else
    {
        foot = RIGHT_FOOT;
    }

    re2uta::CirclePathStepGenerator generator( src, m_dest, halfStepSize, halfStanceWidth, foot);

    int i;
    static const int MAX_ITR = 100000;
    for( i = 0 ; (!generator.hasArrived(rCurrentPose) || !generator.hasArrived(lCurrentPose)) && i<MAX_ITR && ros::ok(); ++i )
    {
        if( i % 2 )
        {
            lCurrentPose = generator.generateNextStep( rCurrentPose, lCurrentPose, i );
            tf::poseEigenToMsg( lCurrentPose, poseStamped.pose );
            lFootTraj.poses.push_back( poseStamped );
        }
        else
        {
            rCurrentPose = generator.generateNextStep( lCurrentPose, rCurrentPose, i );
            tf::poseEigenToMsg( rCurrentPose, poseStamped.pose );
            rFootTraj.poses.push_back( poseStamped );
        }
    }
    if(i>=MAX_ITR)
    {
        ROS_WARN("[WalkingHandler - DEBUG] Couldn't generate foot trajectories properly on OCU side... Feeling lucky?");
    }

    lFootTrajPub.publish( lFootTraj );
    rFootTrajPub.publish( rFootTraj );

    ROS_INFO("[WalkingHandler] Finished making circle path");
}

void WalkingHandler::makeNaoPath()
{
    ROS_INFO("[WalkingHandler] Making nao path");
    // TODO
}

void WalkingHandler::makeStepPath( bool useCurrent )
{
    ROS_INFO("[WalkingHandler] Making manual path");

    bool leftFoot;
    int stepIndex = 0;
    visualization_msgs::MarkerArray markerArrayMsg;
    visualization_msgs::Marker      markerMsg;
    markerMsg.action  = visualization_msgs::Marker::MODIFY;
    markerMsg.type    = visualization_msgs::Marker::CUBE;
    markerMsg.scale.x = 0.2;
    markerMsg.scale.y = 0.1;
    markerMsg.scale.z = 0.001;
    markerMsg.header.frame_id = "/odom";
    markerMsg.frame_locked    = true;

    leftFoot = (std::strcmp(m_currentFoot.c_str(),FT_LEFT_STR.c_str())==0);

    for(int i=0; i<m_queuedMsgs.size(); ++i)
    {
        markerMsg.pose = m_queuedMsgs.at(i).pose;
        markerMsg.id = stepIndex;
        stepIndex++;

        if( leftFoot )
        {
            Eigen::Map<Eigen::Vector4f>( &markerMsg.color.r ) = Eigen::Vector4f( 1, 0, 0, 1 );
        }
        else
        {
            Eigen::Map<Eigen::Vector4f>( &markerMsg.color.r ) = Eigen::Vector4f( 0, 0, 1, 1 );
        }
        leftFoot = !leftFoot;

        markerArrayMsg.markers.push_back( markerMsg );
//        ROS_INFO_STREAM("[WalkingHandler] I just drew footstep "<< i);
    }

    if(useCurrent)
    {
        markerMsg.pose = m_latestPoseMsg.pose;
        markerMsg.id = stepIndex;

        if( leftFoot )
        {
            Eigen::Map<Eigen::Vector4f>( &markerMsg.color.r ) = Eigen::Vector4f( 1, 0.6, 0.6, 1 );
        }
        else
        {
            Eigen::Map<Eigen::Vector4f>( &markerMsg.color.r ) = Eigen::Vector4f( 0.6, 0.6, 1, 1 );
        }

        markerArrayMsg.markers.push_back( markerMsg );
    }

    m_footstepMarkerPub.publish( markerArrayMsg );
}
