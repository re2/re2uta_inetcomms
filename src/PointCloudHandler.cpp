/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <re2uta/PointCloudHandler.h>
#include <re2uta/ReducedMsgController.h>

#include <sensor_msgs/PointCloud2.h>
#include <sensor_msgs/PointCloud.h>
#include <pcl/ros/conversions.h>
#include <sensor_msgs/point_cloud_conversion.h>


#include "laser_assembler/AssembleScans.h"
#include <tf/transform_listener.h>
#include <tf_conversions/tf_eigen.h>
#include <pcl_ros/transforms.h>

#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/passthrough.h>
//#include <pcl/common/impl/transforms.hpp>

using namespace re2uta;

static const float EPSILON = 0.0001;

/**
 * Constructor takes in a ros node and a pointer to the ReducedMsgController
 */
PointCloudHandler::PointCloudHandler( ros::NodeHandle node, ReducedMsgController *rmc )
{
    m_node = node;
    m_rmc = rmc;

    setup();
}

PointCloudHandler::~PointCloudHandler()
{
}

/**
 * Initialize the handler.
 */
void PointCloudHandler::setup()
{
    // Grab the params from the param server (set via the launch file)
    int tempHandlerId;
    bool client;
    m_node.param<int>(         "/re2uta/PointCloudHandler_id",                    tempHandlerId,        0                           );
    m_node.param<std::string>( "/re2uta/PointCloudHandler_rosTopicUIData",        m_rosTopicPubVal,     "/ui/pointCloudHandlerData" );
    m_node.param<std::string>( "/re2uta/PointCloudHandler_rosTopicUIReq",         m_rosTopicSubUIVal,   "/ui/pointCloudHandlerReq"  );
    m_node.param<std::string>( "/re2uta/PointCloudHandler_rosTopicAtlasHeadData", m_rosTopicSubDataVal, "/multisense_sl/laser/scan" );
//    /multisense_sl/laser/scan
    m_node.param<bool>( "/re2uta/RMC_client", client, false );

    m_handlerId = tempHandlerId;

    m_collectPoints = false;

    // Only needed if field comp
    if(!client)
    {
        ROS_INFO("[PointCloudHandler] Waiting for [assemble_scans] to be advertised");
        ros::service::waitForService("assemble_scans");
        ROS_INFO("[PointCloudHandler] Found assemble_scans service!");
    }

    // Setup the publisher and subscriber
    std::string rosTopicPrevPubVal = m_rosTopicPubVal + "Prev";
    m_uiPub = m_node.advertise< sensor_msgs::PointCloud2 >( m_rosTopicPubVal, 1);
    m_uiPrevPub = m_node.advertise< sensor_msgs::PointCloud2 >( rosTopicPrevPubVal, 1);
    m_uiSub = m_node.subscribe( m_rosTopicSubUIVal, 1, &PointCloudHandler::requestData, this );

    m_tfListener.setExtrapolationLimit(ros::Duration(0.1));

    // Create the service client for calling the assembler
//    client_ = m_node.serviceClient<laser_assembler::AssembleScans>("atlas_multisense_assembler");
    client_ = m_node.serviceClient<laser_assembler::AssembleScans>("assemble_scans");

    setCommLvl(COMM_LVL_SMALLEST);
}

/**
 * Uncompresses, hydrates, or otherwise unpacks the data (if applicable), then
 * publishes it on the appropriate ROS topic.
 */
void PointCloudHandler::unpack( unsigned char* data )
{
    std::stringstream strData;
    std::stringstream numPointsSS;
    int numPoints = 0;
    int delimLocBegin;
    int delimLocEnd;

    ROS_INFO("[PointCloudHandler] Unpacking data:");

//    for(int i=0; i<15; i++)
//    {
//        ROS_INFO("[PointCloudHandler]   %d",data[i]);
//    }

    ROS_INFO( "[PointCloudHandler]    Extracting point cloud info" );
    strData << (data);

    // Extract the number of points
    delimLocBegin = 0;
    delimLocEnd = strData.str().find( "|" );
    if ( delimLocBegin != delimLocEnd )
    {
        numPointsSS << strData.str().substr( delimLocBegin, delimLocEnd );
        numPointsSS >> numPoints;
    }

    ROS_INFO("[PointCloudHandler]    Unpacking point cloud with %d points", numPoints);

    if(numPoints > 0)
    {
        m_points.clear();

        delimLocBegin = delimLocEnd + 1;
        delimLocEnd   = delimLocBegin + 3;

        while(delimLocEnd < (numPoints*3*3) )
        {
            PointType p;
            char baseOneTwentySevenNum[3];

            {
                baseOneTwentySevenNum[0] = data[delimLocBegin];
                baseOneTwentySevenNum[1] = data[delimLocBegin+1];
                baseOneTwentySevenNum[2] = data[delimLocBegin+2];

                float x = toBaseTen(baseOneTwentySevenNum);
                p.x = x;

                delimLocBegin = delimLocEnd;
                delimLocEnd = delimLocBegin + 3;
            }

            if(delimLocEnd < (numPoints*3*3) )
            {
                baseOneTwentySevenNum[0] = data[delimLocBegin];
                baseOneTwentySevenNum[1] = data[delimLocBegin+1];
                baseOneTwentySevenNum[2] = data[delimLocBegin+2];

                float y = toBaseTen(baseOneTwentySevenNum);
                p.y = y;

                delimLocBegin = delimLocEnd;
                delimLocEnd = delimLocBegin + 3;
            }
            else
            {
                break; // We didn't get all the info for a point, so break out of loop
            }

            if(delimLocEnd < (numPoints*3*3) )
            {
                baseOneTwentySevenNum[0] = data[delimLocBegin];
                baseOneTwentySevenNum[1] = data[delimLocBegin+1];
                baseOneTwentySevenNum[2] = data[delimLocBegin+2];

                float z = toBaseTen(baseOneTwentySevenNum);
                p.z = z;

                delimLocBegin = delimLocEnd;
                delimLocEnd = delimLocBegin + 3;
            }
            else
            {
                break; // We didn't get all the info for a point, so break out of loop
            }

            m_points.push_back(p);
        }
    }

    publish();
}

/**
 * Compress, dilute, or otherwise reduce the data, then hand it off to the
 * ReducedMsgController to be sent across the line/internet.
 */
void PointCloudHandler::pack( char* params )
{
    std::stringstream tempBuf;

    ROS_INFO("[PointCloudHandler] Received req with params:");
//    for(int i=0; i<19; ++i)
//    {
//        ROS_INFO( "[PointCloudHandler]   %d",((int)params[i]) );
//    }

    PointType p;
    int paramsIndex = 0;

    char baseOneTwentySevenNumber[3];

    if(params[0])
    {
        int param = (int)params[paramsIndex]; // 0
        paramsIndex++;
        int commLvl = param;

        setCommLvl(commLvl);

        // TODO: set rotation speed?

        baseOneTwentySevenNumber[0] = params[paramsIndex]; // 1
        paramsIndex++;
        baseOneTwentySevenNumber[1] = params[paramsIndex]; // 2
        paramsIndex++;
        baseOneTwentySevenNumber[2] = params[paramsIndex]; // 3
        paramsIndex++;
        m_xMin = toBaseTen(baseOneTwentySevenNumber);

        baseOneTwentySevenNumber[0] = params[paramsIndex]; // 4
        paramsIndex++;
        baseOneTwentySevenNumber[1] = params[paramsIndex]; // 5
        paramsIndex++;
        baseOneTwentySevenNumber[2] = params[paramsIndex]; // 6
        paramsIndex++;
        m_xMax = toBaseTen(baseOneTwentySevenNumber);

        baseOneTwentySevenNumber[0] = params[paramsIndex]; // 7
        paramsIndex++;
        baseOneTwentySevenNumber[1] = params[paramsIndex]; // 8
        paramsIndex++;
        baseOneTwentySevenNumber[2] = params[paramsIndex]; // 9
        paramsIndex++;
        m_yMin = toBaseTen(baseOneTwentySevenNumber);

        baseOneTwentySevenNumber[0] = params[paramsIndex]; // 10
        paramsIndex++;
        baseOneTwentySevenNumber[1] = params[paramsIndex]; // 11
        paramsIndex++;
        baseOneTwentySevenNumber[2] = params[paramsIndex]; // 12
        paramsIndex++;
        m_yMax = toBaseTen(baseOneTwentySevenNumber);

        baseOneTwentySevenNumber[0] = params[paramsIndex]; // 13
        paramsIndex++;
        baseOneTwentySevenNumber[1] = params[paramsIndex]; // 14
        paramsIndex++;
        baseOneTwentySevenNumber[2] = params[paramsIndex]; // 15
        paramsIndex++;
        m_zMin = toBaseTen(baseOneTwentySevenNumber);

        baseOneTwentySevenNumber[0] = params[paramsIndex]; // 16
        paramsIndex++;
        baseOneTwentySevenNumber[1] = params[paramsIndex]; // 17
        paramsIndex++;
        baseOneTwentySevenNumber[2] = params[paramsIndex]; // 18
        paramsIndex++;
        m_zMax = toBaseTen(baseOneTwentySevenNumber);
    }
    else
    {
        ROS_INFO("[PointCloudHandler] No params given, using previous params");
    }

    ROS_INFO("[PointCloudHandler]    x min/max %f/%f",m_xMin, m_xMax);
    ROS_INFO("[PointCloudHandler]    y min/max %f/%f",m_yMin, m_yMax);
    ROS_INFO("[PointCloudHandler]    z min/max %f/%f",m_zMin, m_zMax);

    // Request the point cloud from the laser_assembler service
    laser_assembler::AssembleScans srv;
    // TODO: adjust duration based on laser rotation
    srv.request.begin = ( ros::Time::now() - ros::Duration( 4,0 ) ); // 3 seconds ago
    srv.request.end   = ros::Time::now();

    ROS_INFO("[PointCloudHandler] Grabbing points between %f and %f", srv.request.begin.toSec(), srv.request.end.toSec());

    // Make the service call
    if (client_.call(srv))
    {
        ROS_INFO("[PointCloudHandler] Got cloud with %d points", srv.response.cloud.points.size());
        sensor_msgs::PointCloud2 tempCloudMsg;
        sensor_msgs::convertPointCloudToPointCloud2(srv.response.cloud, tempCloudMsg);
        pcl::fromROSMsg(tempCloudMsg, m_points);
        ROS_INFO("[PointCloudHandler] Processing %d points", m_points.points.size());
    }
    else
    {
        ROS_ERROR("Error making service call\n") ;
    }

    // Compress point cloud based on m_commLvl
    ROS_INFO( "[PointCloudHandler] Converting point cloud using comm level %d", m_commLvl );
    switch ( m_commLvl )
    {
        case COMM_LVL_SMALLEST:
            convertSmallest();
            break;
        case COMM_LVL_SMALL:
            convertSmall();
            break;
        case COMM_LVL_MEDIUM:
            convertMedium();
            break;
        case COMM_LVL_LARGE:
            convertLarge();
            break;
        case COMM_LVL_LARGEST:
            convertLargest();
            break;
        default:
            ROS_WARN( "[PointCloudHandler] Unknown comm level in pack(): %d", m_commLvl );
            convertSmallest();
            break;
    }

    // Determine the number of points in the cloud and add to outgoing msg
    tempBuf << m_points.points.size() << "|";
    ROS_INFO( "[PointCloudHandler] %d points to be packed", m_points.points.size() );

    ROS_INFO( "[PointCloudHandler] Starting to pack point cloud" );
    int dataOutSize = (m_points.points.size()*3*3) + (tempBuf.str().size());
    char* dataOut = new char[dataOutSize];
    int bufIndex = (tempBuf.str().size());

    strcpy(dataOut, tempBuf.str().c_str());

    // TODO: Is there a better way to do this with less bits?
    for(int i=0; i<m_points.points.size(); ++i)
    {
        // Grab a point
        p = m_points[i];

        // For each dimension (x,y,z)...

        // p.x
        toBaseOneTwentySeven(p.x, baseOneTwentySevenNumber);
        // Add to string stream
        dataOut[bufIndex] = baseOneTwentySevenNumber[0];
        bufIndex++;
        dataOut[bufIndex] = baseOneTwentySevenNumber[1];
        bufIndex++;
        dataOut[bufIndex] = baseOneTwentySevenNumber[2];
        bufIndex++;

        // p.y
        toBaseOneTwentySeven(p.y, baseOneTwentySevenNumber);
        // Add to string stream
        dataOut[bufIndex] = baseOneTwentySevenNumber[0];
        bufIndex++;
        dataOut[bufIndex] = baseOneTwentySevenNumber[1];
        bufIndex++;
        dataOut[bufIndex] = baseOneTwentySevenNumber[2];
        bufIndex++;

        // p.z
        toBaseOneTwentySeven(p.z, baseOneTwentySevenNumber);
        // Add to string stream
        dataOut[bufIndex] = baseOneTwentySevenNumber[0];
        bufIndex++;
        dataOut[bufIndex] = baseOneTwentySevenNumber[1];
        bufIndex++;
        dataOut[bufIndex] = baseOneTwentySevenNumber[2];
        bufIndex++;

    }

    ROS_INFO( "[PointCloudHandler] Data packed. Sending point cloud!" );

    m_rmc->send( (m_handlerId + 1), dataOut, dataOutSize );
}

/**
 *
 */
void PointCloudHandler::toBaseOneTwentySeven(float baseTenNum, char* baseOneTwentySevenNumber)
{
    int baseTenDecimalShifted;
    int baseOneTwentySevenDigit;

    baseTenDecimalShifted = (int)((baseTenNum*(pow(10,NUM_DIGITS_TO_KEEP)))+0.00003); // +0.000001 because computers are stupid

    for(int exponent=0; exponent<3; ++exponent) // Always have three digits per value
    {
        baseOneTwentySevenDigit = (( (int)( baseTenDecimalShifted/(pow(BASE,exponent)) ) ) % BASE);

        baseOneTwentySevenNumber[2-exponent] = (char)baseOneTwentySevenDigit;
    }
}

/**
 *
 */
float PointCloudHandler::toBaseTen(char* baseOneTwentySevenNumber)
{
    float baseTenNumber = 0;
    int digitIndex = 0;
    int baseOneTwentySevenDigit;

    for(int exponent=2; exponent>=0; --exponent) // Always have three digits per value
    {
        baseOneTwentySevenDigit = (int)(baseOneTwentySevenNumber[digitIndex]);
        ++digitIndex;

        baseTenNumber += ((baseOneTwentySevenDigit ) * pow(BASE,exponent))*pow(10, -1*NUM_DIGITS_TO_KEEP);
    }

    return baseTenNumber;
}

/**
 * Filter out points from the input cloud given the field (usually x, y, or z) and the
 * min/max values and store the remaining points in the output cloud
 */
void PointCloudHandler::trimPointLocation( const PointCloud::ConstPtr & inputCloud,
                                      PointCloud::Ptr            & outputCloud,
                                      std::string fieldName, double minValue, double maxValue, bool setNegative )
{
    // Build a passthrough filter to remove points we don't want
    ::pcl::PassThrough<PointType> pass;

    pass.setInputCloud( inputCloud );
    pass.setFilterFieldName( fieldName );
    pass.setFilterLimitsNegative( setNegative );
    pass.setFilterLimits( minValue, maxValue );

    pass.filter( *outputCloud );
}

/**
 * Trim points based on proximity to each other
 */
void PointCloudHandler::trimPointSpacing(float leafSize)
{
    sensor_msgs::PointCloud2::Ptr  cloudVoxelGridmsg( new sensor_msgs::PointCloud2() );
    sensor_msgs::PointCloud2::Ptr  cloudPostVoxelGridMsg(    new sensor_msgs::PointCloud2() );
    
    ::pcl::PointCloud<PointType>::Ptr tempCloud( new ::pcl::PointCloud<PointType> );
    ::pcl::PointCloud<PointType>::Ptr cloudReducedZ(    new ::pcl::PointCloud<PointType> );
    ::pcl::PointCloud<PointType>::Ptr cloudReducedYZ(   new ::pcl::PointCloud<PointType> );
    ::pcl::PointCloud<PointType>::Ptr cloudReducedXYZ(  new ::pcl::PointCloud<PointType> );

    
    if( m_points.points.size() > 0 )
    {
        ::pcl::toROSMsg( m_points, *cloudVoxelGridmsg );

        // Filter points (point reduction)
        ::pcl::VoxelGrid<sensor_msgs::PointCloud2> sor;
        sor.setInputCloud( cloudVoxelGridmsg );

        sor.setLeafSize( leafSize, leafSize, leafSize );

        // Filter
        sor.filter( *cloudPostVoxelGridMsg );

        // Convert back
        ::pcl::fromROSMsg( *cloudPostVoxelGridMsg, m_points );

        *tempCloud = m_points;

        trimPointLocation( tempCloud,
                      cloudReducedZ,
                      "z", m_zMin, m_zMax, false );

        trimPointLocation( cloudReducedZ,
                      cloudReducedYZ,
                      "y", m_yMin, m_yMax, false );

        trimPointLocation( cloudReducedYZ,
                      cloudReducedXYZ,
                      "x", m_xMin, m_xMax, false );

        m_points = *cloudReducedXYZ;
    }
    else
    {
        ROS_INFO("No points remaining...");
    }

    ROS_INFO("[PointCloudHandler] Finished point reduction!");
}

/**
 */
void PointCloudHandler::convertSmallest()
{
    trimPointSpacing(0.8); // Larger leaf size = less points
}

/**
 */
void PointCloudHandler::convertSmall()
{
    trimPointSpacing(0.4); // Larger leaf size = less points
}

/**
 */
void PointCloudHandler::convertMedium()
{
    trimPointSpacing(0.1); // Larger leaf size = less points
}

/**
 */
void PointCloudHandler::convertLarge()
{
    trimPointSpacing(0.05); // Larger leaf size = less points
}

/**
 */
void PointCloudHandler::convertLargest()
{
    trimPointSpacing(0.01); // Larger leaf size = less points
}

/**
 * Publish the most recent data on the appropriate ROS topic. This data
 * might be published once or published repeatedly via a timer.
 */
void PointCloudHandler::publish()
{
    sensor_msgs::PointCloud2 pointsMsg;
    Eigen::Affine3d src;
    tf::StampedTransform currentOdom;

    m_tfListener.lookupTransform("/odom", "/pelvis",
            ros::Time(0), currentOdom); // Time 0 = latest
    tf::TransformTFToEigen(currentOdom, src);

    m_points.header.frame_id = "/pelvis";

    pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloud_in( new pcl::PointCloud<pcl::PointXYZ>(m_points));
    pcl::transformPointCloud(*cloud_in, m_points, src.cast<float>() );

    m_points.header.frame_id = "/odom";
    pcl::toROSMsg(m_points, pointsMsg);

    // Publish the data
    m_uiPub.publish(pointsMsg);


    // Re-publish to prev points before updating
    m_uiPrevPub.publish(m_prevPointsMsg);
    m_points.header.frame_id = "/odom";
    pcl::toROSMsg(m_points, m_prevPointsMsg);
}

bool PointCloudHandler::compareFloats (float a, float b)
{
    bool returnVal;
    float diff = a - b;

    returnVal = (diff < EPSILON) && (diff > -EPSILON);

//    if(returnVal)
//    {
//        ROS_INFO("[PointCloudHandler] Comparing %f vs %f (diff %f) (Ep %f) - TRUE", a, b, diff, EPSILON);
//    }
//    else
//    {
//        ROS_INFO("[PointCloudHandler] Comparing %f vs %f (diff %f) (Ep %f) - FALSE", a, b, diff, EPSILON);
//    }

    return returnVal;
}

/**
 * Send a request to the ReducedMsgController requesting data. This should
 * be a single char (the id) as an odd number. More info can be specified
 * if you wish to only get a subset of data.
 */
void PointCloudHandler::requestData( const re2uta_inetComms::ReqPointCloudData& msg )
{
    ROS_INFO( "[PointCloudHandler] Received request for a point cloud from UI. Sending req to RMC." );

    // TODO: Fix so we can add additional params (such as the level of detail we want)
    // Level of detail (commLvl)
    char bufWithParams[19]; // commLvl(1 char) + 3 chars for each min/max(3*6 chars)

    int bufIndex = 0;
    int bufIntVal;
    char baseOneTwentySevenNumber[3];

    m_points.clear();

    if(msg.commLvl == m_commLvl
       && compareFloats(msg.xMin, m_xMin)
       && compareFloats(msg.xMax, m_xMax)
       && compareFloats(msg.yMin, m_yMin)
       && compareFloats(msg.yMax, m_yMax)
       && compareFloats(msg.zMin, m_zMin)
       && compareFloats(msg.zMax, m_zMax) )
    {
        ROS_INFO("[PointCloudHandler] Reusing params (%d, %f, %f, %f, %f, %f, %f)",
                 m_commLvl,
                 m_xMin,
                 m_xMax,
                 m_yMin,
                 m_yMax,
                 m_zMin,
                 m_zMax);
        m_rmc->send( m_handlerId, NULL, 0 );
    }
    else
    {
//        ROS_INFO("[PointCloudHandler] Comparing params vars: (%d, %f, %f, %f, %f, %f, %f)",
//                 m_commLvl,
//                 m_xMin,
//                 m_xMax,
//                 m_yMin,
//                 m_yMax,
//                 m_zMin,
//                 m_zMax);
//        ROS_INFO("[PointCloudHandler] Comparing params msg: (%d, %f, %f, %f, %f, %f, %f)",
//                 msg.commLvl,
//                 msg.xMin,
//                 msg.xMax,
//                 msg.yMin,
//                 msg.yMax,
//                 msg.zMin,
//                 msg.zMax);

        setCommLvl(msg.commLvl);
        m_xMin = msg.xMin;
        m_xMax = msg.xMax;
        m_yMin = msg.yMin;
        m_yMax = msg.yMax;
        m_zMin = msg.zMin;
        m_zMax = msg.zMax;
        ROS_INFO("[PointCloudHandler] Vars set (%d, %f, %f, %f, %f, %f, %f)",
                 m_commLvl,
                 m_xMin,
                 m_xMax,
                 m_yMin,
                 m_yMax,
                 m_zMin,
                 m_zMax);

        bufIntVal =  m_commLvl;

        bufWithParams[bufIndex] = (char)bufIntVal; // 0
        bufIndex++;

        toBaseOneTwentySeven(msg.xMin, baseOneTwentySevenNumber); // &(baseOneTwentySevenNumber[0]) =
        bufWithParams[bufIndex] = baseOneTwentySevenNumber[0];
        bufIndex++;
        bufWithParams[bufIndex] = baseOneTwentySevenNumber[1];
        bufIndex++;
        bufWithParams[bufIndex] = baseOneTwentySevenNumber[2];
        bufIndex++;

        toBaseOneTwentySeven(msg.xMax, baseOneTwentySevenNumber);
        bufWithParams[bufIndex] = baseOneTwentySevenNumber[0];
        bufIndex++;
        bufWithParams[bufIndex] = baseOneTwentySevenNumber[1];
        bufIndex++;
        bufWithParams[bufIndex] = baseOneTwentySevenNumber[2];
        bufIndex++;

        toBaseOneTwentySeven(msg.yMin, baseOneTwentySevenNumber);
        bufWithParams[bufIndex] = baseOneTwentySevenNumber[0];
        bufIndex++;
        bufWithParams[bufIndex] = baseOneTwentySevenNumber[1];
        bufIndex++;
        bufWithParams[bufIndex] = baseOneTwentySevenNumber[2];
        bufIndex++;

        toBaseOneTwentySeven(msg.yMax, baseOneTwentySevenNumber);
        bufWithParams[bufIndex] = baseOneTwentySevenNumber[0];
        bufIndex++;
        bufWithParams[bufIndex] = baseOneTwentySevenNumber[1];
        bufIndex++;
        bufWithParams[bufIndex] = baseOneTwentySevenNumber[2];
        bufIndex++;

        toBaseOneTwentySeven(msg.zMin, baseOneTwentySevenNumber);
        bufWithParams[bufIndex] = baseOneTwentySevenNumber[0];
        bufIndex++;
        bufWithParams[bufIndex] = baseOneTwentySevenNumber[1];
        bufIndex++;
        bufWithParams[bufIndex] = baseOneTwentySevenNumber[2];
        bufIndex++;

        toBaseOneTwentySeven(msg.zMax, baseOneTwentySevenNumber);
        bufWithParams[bufIndex] = baseOneTwentySevenNumber[0];
        bufIndex++;
        bufWithParams[bufIndex] = baseOneTwentySevenNumber[1];
        bufIndex++;
        bufWithParams[bufIndex] = baseOneTwentySevenNumber[2];
        bufIndex++;

//        ROS_INFO("[PointCloudHandler] Sending req with params:");
//        for(int i=0; i<19; ++i)
//        {
//            ROS_INFO( "[PointCloudHandler]   %d",((int)bufWithParams[i]) );
//        }

        // Send buf
        m_rmc->send( m_handlerId, bufWithParams, 19 );
    }
}
