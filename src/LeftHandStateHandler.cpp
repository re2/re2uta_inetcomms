/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <re2uta/LeftHandStateHandler.h>
#include <re2uta/ReducedMsgController.h>

using namespace re2uta;

/**
 * Constructor takes in a ros node and a pointer to the ReducedMsgController
 */
LeftHandStateHandler::LeftHandStateHandler( ros::NodeHandle node, ReducedMsgController *rmc )
{
    m_node = node;
    m_rmc = rmc;

    setup();
}

LeftHandStateHandler::~LeftHandStateHandler()
{

}

/**
 * Receive the data from a ros topic (a callback function) and store it.
 * Note, just because we receive data does not mean we always want to send
 * that data over the line. Store the data until a request is made for that
 * data to be sent over the line or, if a timer is setup, when the timer
 * expires signifying the data should be sent.
 */
void LeftHandStateHandler::rcvJSDataCB( const sensor_msgs::JointState& msg )
{
    m_rosLeftHandState.position = msg.position;
}

/**
 * Uncompresses, hydrates, or otherwise unpacks the data (if applicable), then
 * publishes it on the appropriate ROS topic.
 */
void LeftHandStateHandler::unpack( unsigned char* data )
{
    for ( int i = 0 ; i < 12 ; i++ )
    {
        m_packedLeftHandState[i] = * ((unsigned short *)& (data[2 * i ]));

        m_rosLeftHandState.position[i] = (double) (m_packedLeftHandState[i]) * 1e-4 - M_PI;
    }

    // Publish the data
    publish();
}

/**
 * Initialize the handler.
 */
void LeftHandStateHandler::setup()
{
    // Grab the params from the param server (set via the launch file)
    int tempHandlerId;
    m_node.param<int>("/re2uta/LeftHandStateHandler_id", tempHandlerId, 0);

    // the topic that is published from the data handler to the OCU
    m_node.param<std::string>("/re2uta/LeftHandStateHandler_rosTopicUIData", m_rosTopicPubVal, "/ui/leftHandStateHandlerData");
    // the topic that is published from the OCU to the data handler
    m_node.param<std::string>("/re2uta/LeftHandStateHandler_rosTopicUIReq", m_rosTopicSubUIVal, "/ui/leftHandStateHandlerReq");
    // the topic that is published from the simulator to the data handler
    m_node.param<std::string>("/re2uta/LeftHandStateHandler_rosTopicLeftHandStateData", m_rosTopicSubDataVal, 
                              "/sandia_hands/l_hand/joint_states");

    m_handlerId = tempHandlerId;

    // Setup the publisher and subscriber
    m_uiPub = m_node.advertise< sensor_msgs::JointState  >( m_rosTopicPubVal, 1);
    m_uiSub = m_node.subscribe( m_rosTopicSubUIVal, 1, &LeftHandStateHandler::requestData, this );
    m_jsSub = m_node.subscribe( m_rosTopicSubDataVal, 1, &LeftHandStateHandler::rcvJSDataCB, this );

    m_rosLeftHandState.position.resize(12);
}

/**
 * Compress, dilute, or otherwise reduce the data, then hand it off to the
 * ReducedMsgController to be sent across the line/internet.
 */
void LeftHandStateHandler::pack( char* params )
{
    char buf[24];

    // convert the joint states into shorts
    for ( int i = 0 ; i < 12 ; i++ )
    {
        m_packedLeftHandState[i] = (unsigned short) (10000 * (m_rosLeftHandState.position[i] + M_PI));
        * ((unsigned short *)& (buf[2*i])) = m_packedLeftHandState[i];
    }

    m_rmc->send((m_handlerId+1), buf, 24);
}

/**
 * Publish the most recent data on the appropriate ROS topic. This data
 * might be published once or published repeatedly via a timer.
 */
void LeftHandStateHandler::publish()
{
    // Publish the data
    m_uiPub.publish(m_rosLeftHandState);
}

/**
 * Send a request to the ReducedMsgController requesting data. This should
 * be a single char (the id) as an odd number. More info can be specified
 * if you wish to only get a subset of data.
 */
void LeftHandStateHandler::requestData(const std_msgs::Empty& msg )
{
    m_rmc->send(m_handlerId, NULL, 0);
}

void LeftHandStateHandler::convertSmallest()
{
}
void LeftHandStateHandler::convertSmall()
{
}
void LeftHandStateHandler::convertMedium()
{
}
void LeftHandStateHandler::convertLarge()
{
}
void LeftHandStateHandler::convertLargest()
{
}
