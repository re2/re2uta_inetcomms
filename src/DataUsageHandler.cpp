/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <re2uta/DataUsageHandler.h>
#include <re2uta/ReducedMsgController.h>

using namespace re2uta;

/**
 * Constructor takes in a ros node and a pointer to the ReducedMsgController
 */
DataUsageHandler::DataUsageHandler( ros::NodeHandle node, ReducedMsgController *rmc )
{
    m_node = node;
    m_rmc = rmc;

    setup();
}

DataUsageHandler::~DataUsageHandler()
{

}

/**
 * Receive the data from a ros topic (a callback function) and store it.
 * Note, just because we receive data does not mean we always want to send
 * that data over the line. Store the data until a request is made for that
 * data to be sent over the line or, if a timer is setup, when the timer
 * expires signifying the data should be sent.
 */
void DataUsageHandler::rcvUplinkDataCB( const std_msgs::String& msg )
{
    m_uplink = msg;
}

void DataUsageHandler::rcvDownlinkDataCB( const std_msgs::String& msg )
{
    m_downlink = msg;
}

/**
 * Initialize the handler.
 */
void DataUsageHandler::setup()
{
    // Grab the params from the param server (set via the launch file)
    int tempHandlerId;
    m_node.param<int>("/re2uta/DataUsageHandler_id", tempHandlerId, 0);

    // the topic that is published from the data handler to the OCU
    m_node.param<std::string>("/re2uta/DataUsageHandler_rosTopicUIUplinkData", m_rosTopicPubVal, 
                              "/ui/DataUsageHandlerUplinkData");

    m_node.param<std::string>("/re2uta/DataUsageHandler_rosTopicUIUplinkData", m_rosTopicPubVal2, 
                              "/ui/DataUsageHandlerDownlinkData");

    // the topic that is published from the OCU to the data handler
    m_node.param<std::string>("/re2uta/DataUsageHandler_rosTopicUIReq", m_rosTopicSubUIVal, 
                              "/ui/DataUsageHandlerReq");

    // the topic that is published from the simulator to the data handler
    m_node.param<std::string>("/re2uta/DataUsageHandler_rosTopicUplinkData", m_rosTopicSubDataVal,
                              "/vrc/bytes/remaining/uplink");

    m_node.param<std::string>("/re2uta/DataUsageHandler_rosTopicDownlinkData", m_rosTopicSubDataVal2,
                              "/vrc/bytes/remaining/downlink");

    m_handlerId = tempHandlerId;

    // Setup the publisher and subscriber
    m_uiUplinkPub = m_node.advertise< std_msgs::String  >( m_rosTopicPubVal, 1);
    m_uiDownlinkPub = m_node.advertise< std_msgs::String  >( m_rosTopicPubVal2, 1);

    m_uiSub = m_node.subscribe( m_rosTopicSubUIVal, 1, &DataUsageHandler::requestData, this );

    m_uplinkSub = m_node.subscribe( m_rosTopicSubDataVal, 1, &DataUsageHandler::rcvUplinkDataCB, this );
    m_downlinkSub = m_node.subscribe( m_rosTopicSubDataVal2, 1, 
                                      &DataUsageHandler::rcvDownlinkDataCB, this );
}

/**
 * Compress, dilute, or otherwise reduce the data, then hand it off to the
 * ReducedMsgController to be sent across the line/internet.
 */
void DataUsageHandler::pack( char* params )
{
    char buf[8];
    unsigned int tempIntUp;
    unsigned int tempIntDn;

    // uplink
    if (!(std::istringstream(m_uplink.data) >> tempIntUp)) tempIntUp = 0;
    *((unsigned int *)& (buf[0])) = tempIntUp;

    // downlink
    if (!(std::istringstream(m_downlink.data) >> tempIntDn)) tempIntDn = 0;

    *((unsigned int *)& (buf[4])) = tempIntDn;

    ROS_INFO("[DataUsageHandler - PACK]: Uplink: %s %d",  m_uplink.data.c_str(), tempIntUp);
    ROS_INFO("[DataUsageHandler - PACK]: Downlink: %s %d",  m_downlink.data.c_str(), tempIntDn);

    for (int i=0; i<8; i++) {
        ROS_INFO("[DataUsageHandler]: Sending [%d] = %d", i, buf[i]);
    }

    m_rmc->send((m_handlerId+1), buf, 8);
}

/**
 * Uncompresses, hydrates, or otherwise unpacks the data (if applicable), then
 * publishes it on the appropriate ROS topic.
 */
void DataUsageHandler::unpack( unsigned char* data )
{
    unsigned int tempIntUp;
    unsigned int tempIntDn;

    // uplink
    tempIntUp = *((unsigned int *)& (data[0]));
    std::ostringstream convertUp;
    convertUp << tempIntUp;
    m_uplink.data = convertUp.str();
    
    // downlink
    tempIntDn = *((unsigned int *)& (data[4]));
    std::ostringstream convertDn;
    convertDn << tempIntDn;
    m_downlink.data = convertDn.str();    

    for (int i=0; i<8; i++) {
        ROS_INFO("[DataUsageHandler]: Received [%d] = %d", i, data[i]);
    }

    ROS_INFO("[DataUsageHandler - UNPACK]: Uplink: %s %d",  m_uplink.data.c_str(), tempIntUp);
    ROS_INFO("[DataUsageHandler - UNPACK]: Downlink: %s %d",  m_downlink.data.c_str(), tempIntDn);

    // Publish the data
    // TBD: IT DOESN'T HAVE TO PUBLISH HERE 
    // INSTEAD IT CAN UPDATE THE RMC'S INTERNAL STATS
    // COMMENTED OUT FOR NOW
    m_rmc->setNumBitsSent(tempIntUp);
    m_rmc->setNumBitsRcvd(tempIntDn);
    //    publish();
}


/**
 * Publish the most recent data on the appropriate ROS topic. This data
 * might be published once or published repeatedly via a timer.
 */
void DataUsageHandler::publish()
{
    // Publish the data
    m_uiUplinkPub.publish(m_uplink);
    m_uiDownlinkPub.publish(m_downlink);
}


/**
 * Send a request to the ReducedMsgController requesting data. This should
 * be a single char (the id) as an odd number. More info can be specified
 * if you wish to only get a subset of data.
 */
void DataUsageHandler::requestData(const std_msgs::Empty& msg )
{
    m_rmc->send(m_handlerId, NULL, 0);
}

void DataUsageHandler::convertSmallest()
{
}
void DataUsageHandler::convertSmall()
{
}
void DataUsageHandler::convertMedium()
{
}
void DataUsageHandler::convertLarge()
{
}
void DataUsageHandler::convertLargest()
{
}
