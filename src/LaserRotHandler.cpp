/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <re2uta/LaserRotHandler.h>

using namespace re2uta;

static const double LR_EPSILON = 0.0001;

/**
 * Constructor takes in a ros node and a pointer to the ReducedMsgController
 */
LaserRotHandler::LaserRotHandler( ros::NodeHandle node, ReducedMsgController *rmc )
{
    m_node = node;
    m_rmc = rmc;

    setup();
}

LaserRotHandler::~LaserRotHandler()
{
}

/**
 * Initialize the handler.
 */
void LaserRotHandler::setup()
{
    // Grab the params from the param server (set via the launch file)
    int tempHandlerId;

    m_node.param<int>(         "/re2uta/LaserRotHandler_id",             tempHandlerId,       0                                  );
    m_node.param<std::string>( "/re2uta/LaserRotHandler_rosTopicPubVal", m_rosTopicPubVal,    "/multisense_sl/set_spindle_speed" );
    m_node.param<std::string>( "/re2uta/LaserRotHandler_rosTopicUIReq",  m_rosTopicSubUIVal,  "/ui/laserRot"                     );
    m_node.param<double>(      "LaserRotHandler_defaultRotSpeed",        m_rotSpeed,          1.2);

    m_handlerId = tempHandlerId;

    m_uiSub = m_node.subscribe( m_rosTopicSubUIVal, 1, &LaserRotHandler::requestData, this );
    m_rotPub = m_node.advertise<std_msgs::Float64>(m_rosTopicPubVal, 1);

    bool client;

    m_node.param<bool>( "/re2uta/RMC_client", client, false );

    if(!client)
    {
        m_timer = m_node.createTimer(ros::Duration(0.5), &LaserRotHandler::initialRotCallback, this);
    }
}

/**
 * Uncompresses, hydrates, or otherwise unpacks the data (if applicable), then
 * publishes it on the appropriate ROS topic.
 */
void LaserRotHandler::unpack( unsigned char* data )
{
    // NA
}

/**
 * Compress, dilute, or otherwise reduce the data, then hand it off to the
 * ReducedMsgController to be sent across the line/internet.
 */
void LaserRotHandler::pack( char* params )
{
    int rotSpeedInt; // keep 1 decimal place

    rotSpeedInt = params[0];
    m_rotSpeed = rotSpeedInt / 10.0;

    publish();
}

/**
 */
void LaserRotHandler::convertSmallest(){/*NA*/}

/**
 */
void LaserRotHandler::convertSmall(){/*NA*/}

/**
 */
void LaserRotHandler::convertMedium(){/*NA*/}

/**
 */
void LaserRotHandler::convertLarge(){/*NA*/}

/**
 */
void LaserRotHandler::convertLargest(){/*NA*/}

/**
 * Publish the most recent data on the appropriate ROS topic. This data
 * might be published once or published repeatedly via a timer.
 */
void LaserRotHandler::publish()
{
    std_msgs::Float64 msg;
    msg.data = m_rotSpeed;

    // Publish the data
    m_rotPub.publish(msg);

    ROS_INFO("[LaserRotHandler] Laser rotation set to a rate of %f",m_rotSpeed);
}

void LaserRotHandler::initialRotCallback(const ros::TimerEvent&)
{
    if(m_rotPub.getNumSubscribers() > 0)
    {
        publish();
        m_timer.stop();
    }
    else
    {
        ROS_INFO_ONCE("[LaserRotHandler] Waiting for subscribers");
    }
}

bool LaserRotHandler::compareDoubles (double a, double b)
{
    bool returnVal;
    double diff = a - b;

    returnVal = (diff < LR_EPSILON) && (diff > -LR_EPSILON);

    return returnVal;
}

/**
 * Send a request to the ReducedMsgController requesting data. This should
 * be a single char (the id) as an odd number. More info can be specified
 * if you wish to only get a subset of data.
 */
void LaserRotHandler::requestData( const std_msgs::Float64& msg )
{
    if(!compareDoubles(m_rotSpeed,msg.data))
    {
        int bufSize = 1;
        char buf[bufSize]; // commLvl(1 char) + 3 chars for each min/max(3*6 chars)

        double rotSpeedDouble = msg.data; // 0-5
        int rotSpeedInt = rotSpeedDouble * 10; // keep 1 decimal place
        buf[0] = rotSpeedInt;

        m_rotSpeed = rotSpeedDouble;

        // Send buf
        m_rmc->send( m_handlerId, buf, bufSize );
    }
    else
    {
        ROS_INFO("[LaserRotHandler] Msg not sent. Laser already set to %f",m_rotSpeed);
    }
}
