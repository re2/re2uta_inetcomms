/*
* COPYRIGHT (C) 2005-2013
* RE2, INC.
* ALL RIGHTS RESERVED
*
*
* THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
* DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
* IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
* OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
* OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
*
* "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
*
* RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
* ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
* RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
* ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
* IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
* OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
*/

#include <re2uta/ObjectTrackingHandler.h>
#include <re2uta/ReducedMsgController.h>

#include <string.h>

using namespace re2uta;

/**
* Constructor takes in a ros node and a pointer to the ReducedMsgController
*/
ObjectTrackingHandler::ObjectTrackingHandler( ros::NodeHandle node, ReducedMsgController *rmc )
{
    m_node = node;
    m_rmc = rmc;

    setup();
}

ObjectTrackingHandler::~ObjectTrackingHandler()
{
}

/**
* Initialize the handler.
*/
void ObjectTrackingHandler::setup()
{
    // Grab the params from the param server (set via the launch file)
    int tempHandlerId;
    m_node.param<int>("/re2uta/ObjectTrackingHandler_id", tempHandlerId, 0);
    m_node.param<std::string>( "/re2uta/ObjectTrackingHandler_rosTopicUIData", m_rosTopicPubVal, "/ui/ObjectTrackingHandlerData");
    m_node.param<std::string>( "/re2uta/ObjectTrackingHandler_rosTopicUIReq", m_rosTopicSubUIVal, "/ui/ObjectTrackingHandlerReq");
    m_node.param<std::string>( "/re2uta/ObjectTrackingHandler_rosTopicObjectData",  m_rosTopicSubDataVal, "/ui/ObjectTrackingHandlerResponse");

    m_handlerId = tempHandlerId;

    // Setup the publisher and subscriber
    m_RectPub = m_node.advertise<re2uta_inetComms::ObjectTrackingInfo>( m_rosTopicPubVal, 1 );
    handle_publish = m_node.advertise<visualization_msgs::Marker>("/ui/hose_marker",1, true);
    pipe_publish = m_node.advertise<visualization_msgs::Marker>("/ui/pipe_marker",1, true);

    m_uiSub = m_node.subscribe( m_rosTopicSubUIVal, 1, &ObjectTrackingHandler::startTracking, this );
    m_dataSub = m_node.subscribe( m_rosTopicSubDataVal, 1, &ObjectTrackingHandler::gotResponse, this);

    setCommLvl(COMM_LVL_SMALLEST);
}

/**
* Send a request to the ReducedMsgController requesting data. This should
* be a single char (the id) as an odd number. More info can be specified
* if you wish to only get a subset of data.
*/
void ObjectTrackingHandler::startTracking( const re2uta_inetComms::ObjectTrackingInfo& msg )
{
    ROS_INFO( "[ObjectTrackingHandler] Received request for tracking" );
    
    char data[9];
    data[0] = msg.obj;
    unsigned short* data2 = (unsigned short*)(data+1);
    data2[0] = msg.data[0];
    data2[1] = msg.data[1];
    data2[2] = msg.data[2];
    data2[3] = msg.data[3];
    
    m_rmc->send( m_handlerId+1, data, 9);
}


void ObjectTrackingHandler::gotResponse( const re2uta_inetComms::ObjectTrackingResponse& msg)
{
    ROS_INFO("Got Response. Returning to OCU");
    char data[29];
    float* data2 = (float*)(data+1);
    data[0] = msg.obj;
    data2[0] = (float)msg.pose.position.x;
    data2[1] = (float)msg.pose.position.y;
    data2[2] = (float)msg.pose.position.z;
    data2[3] = (float)msg.pose.orientation.x;
    data2[4] = (float)msg.pose.orientation.y;
    data2[5] = (float)msg.pose.orientation.z;
    data2[6] = (float)msg.pose.orientation.w;


    m_rmc->send( m_handlerId, data, 29);
}

/**
* Uncompresses, hydrates, or otherwise unpacks the data (if applicable), then
* publishes it on the appropriate ROS topic.
*/
void ObjectTrackingHandler::unpack( unsigned char* data )
{
    ROS_INFO("[ObjectTrackingHandler] Received data. Unpacking...");
    re2uta_inetComms::ObjectTrackingInfo dmsg;
    dmsg.obj = data[0];
    unsigned short* data2 = (unsigned short*)(data+1);
    dmsg.data[0] = data2[0];
    dmsg.data[1] = data2[1];
    dmsg.data[2] = data2[2];
    dmsg.data[3] = data2[3];
    m_RectPub.publish(dmsg);
}
/**
* Publish the most recent data on the appropriate ROS topic. This data
* might be published once or published repeatedly via a timer.
*/
void ObjectTrackingHandler::publish()
{
}

/**
* Compress, dilute, or otherwise reduce the data, then hand it off to the
* ReducedMsgController to be sent across the line/internet.
*/
void ObjectTrackingHandler::publishHandle(geometry_msgs::Pose& pose)
{
    visualization_msgs::Marker handle_m;
    handle_m.header.frame_id = "/left_camera_optical_frame";
    handle_m.header.stamp = ros::Time();
    handle_m.type = visualization_msgs::Marker::CYLINDER;
    handle_m.action = visualization_msgs::Marker::MODIFY;

    handle_m.pose = pose;

    handle_m.scale.x = .05;
    handle_m.scale.y = .05;
    handle_m.scale.z = .1;
    handle_m.color.b = 1;
    handle_m.color.a = 1;

    handle_publish.publish(handle_m);
}
void ObjectTrackingHandler::publishPipe(geometry_msgs::Pose& pose)
{
    visualization_msgs::Marker pipe_m;
    pipe_m.header.frame_id = "/left_camera_optical_frame";
    pipe_m.header.stamp = ros::Time();
    pipe_m.type = visualization_msgs::Marker::ARROW;
    pipe_m.action = visualization_msgs::Marker::MODIFY;
    
    pipe_m.pose = pose;

    pipe_m.scale.x = .35;
    pipe_m.scale.y = .35;
    pipe_m.scale.z = .1;
    pipe_m.color.r = 1;
    pipe_m.color.a = 1;

    pipe_publish.publish(pipe_m);
}


void ObjectTrackingHandler::pack( char* params )
{
    ROS_INFO("Received Tracking data from FC");
    float* data2 = (float*)(params + 1);
    geometry_msgs::Pose p;

    p.position.x  = data2[0];
    p.position.y  = data2[1];
    p.position.z  = data2[2];
    p.orientation.x = data2[3];
    p.orientation.y = data2[4];
    p.orientation.z = data2[5];
    p.orientation.w = data2[6];

    switch(params[0])
    {
        case 0:
        publishHandle(p);
        break;

        case 1:
        publishPipe(p);
        break;
    }
}

void ObjectTrackingHandler::convertSmallest()
{
}
void ObjectTrackingHandler::convertSmall()
{
}
void ObjectTrackingHandler::convertMedium()
{
}
void ObjectTrackingHandler::convertLarge()
{
}
void ObjectTrackingHandler::convertLargest()
{
}
