/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <re2uta/ScoreHandler.h>
#include <re2uta/ReducedMsgController.h>

using namespace re2uta;

/**
 * Constructor takes in a ros node and a pointer to the ReducedMsgController
 */
ScoreHandler::ScoreHandler( ros::NodeHandle node, ReducedMsgController *rmc )
{
    m_node = node;
    m_rmc = rmc;

    setup();
}

ScoreHandler::~ScoreHandler()
{

}

/**
 * Receive the data from a ros topic (a callback function) and store it.
 * Note, just because we receive data does not mean we always want to send
 * that data over the line. Store the data until a request is made for that
 * data to be sent over the line or, if a timer is setup, when the timer
 * expires signifying the data should be sent.
 */
void ScoreHandler::rcvScoreDataCB( const atlas_msgs::VRCScore& msg )
{
    int newT = (int) (msg.sim_time.sec / 60.0);
    int oldT = (int) (m_score.sim_time.sec / 60.0);

    //    ROS_INFO("OLD: %d %d %d", oldT, m_score.completion_score, m_score.falls);
    //    ROS_INFO("NEW: %d %d %d", newT, msg.completion_score, msg.falls);

    // if the minutes have changed, send it
    if (newT > oldT || msg.completion_score > m_score.completion_score || msg.falls > m_score.falls) {
      m_score = msg;
      pack(NULL);
    } 
}

/**
 * Initialize the handler.
 */
void ScoreHandler::setup()
{
    // Grab the params from the param server (set via the launch file)
    int tempHandlerId;
    m_node.param<int>("/re2uta/ScoreHandler_id", tempHandlerId, 0);

    // the topic that is published from the data handler to the OCU
    m_node.param<std::string>("/re2uta/ScoreHandler_rosTopicUIData", m_rosTopicPubVal, 
                              "/ui/ScoreHandlerData");

    // the topic that is published from the OCU to the data handler
    m_node.param<std::string>("/re2uta/ScoreHandler_rosTopicUIReq", m_rosTopicSubUIVal, 
                              "/ui/reqStats");

    // the topic that is published from the simulator to the data handler
    m_node.param<std::string>("/re2uta/ScoreHandler_rosTopicScoreData", m_rosTopicSubDataVal,
                              "/vrc_score");

    m_handlerId = tempHandlerId;

    // Setup the publisher and subscriber
    m_uiPub = m_node.advertise< atlas_msgs::VRCScore  >( m_rosTopicPubVal, 1);
    m_uiSub = m_node.subscribe( m_rosTopicSubUIVal, 1, &ScoreHandler::requestData, this );
    m_scoreSub = m_node.subscribe( m_rosTopicSubDataVal, 1, &ScoreHandler::rcvScoreDataCB, this );

    m_lastSentTime = 0;
}

/**
 * Compress, dilute, or otherwise reduce the data, then hand it off to the
 * ReducedMsgController to be sent across the line/internet.
 */
void ScoreHandler::pack( char* params )
{
    char buf[3];
    double tempTime;
    unsigned short tempShort;

    buf[0] = (char) (m_score.sim_time.sec / 60.0);
    buf[1] = (char) (m_score.completion_score);
    buf[2] = (char) (m_score.falls);

#if 0
    ROS_INFO("[ScoreHandler - PACK]: Wall Time: %d %d", m_score.wall_time.sec, m_score.wall_time.nsec);
    ROS_INFO("[ScoreHandler - PACK]: Sim Time: %d %d", m_score.sim_time.sec, m_score.sim_time.nsec);
    ROS_INFO("[ScoreHandler - PACK]: Wall Time Elapsed: %d %d", m_score.wall_time_elapsed.sec, 
             m_score.wall_time_elapsed.nsec);
    ROS_INFO("[ScoreHandler - PACK]: Sim Time Elapsed: %d %d", m_score.sim_time_elapsed.sec, 
             m_score.sim_time_elapsed.nsec);
    ROS_INFO("[ScoreHandler - PACK]: Completion Score %d", m_score.completion_score);
    ROS_INFO("[ScoreHandler - PACK]: Falls %d", m_score.falls);
    ROS_INFO("[ScoreHandler - PACK]: Task Type %d", m_score.task_type);
#endif
    for (int i=0; i<3; i++) {
        ROS_INFO("[ScoreHandler]: Sending [%d] = %d", i, buf[i]);
    }

    m_rmc->send((m_handlerId+1), buf, 3);
}

/**
 * Uncompresses, hydrates, or otherwise unpacks the data (if applicable), then
 * publishes it on the appropriate ROS topic.
 */
void ScoreHandler::unpack( unsigned char* data )
{
    double tempTime;
    unsigned short tempShort;


    // sim time elapsed
    m_score.sim_time_elapsed.sec = 60 * data[0];
    m_score.sim_time_elapsed.nsec = 0;

    m_score.completion_score = (short) data[1];
    m_score.falls = (short) data[2];

    for (int i=0; i<3; i++) {
        ROS_INFO("[ScoreHandler]: Received [%d] = %d", i, data[i]);
    }

    ROS_INFO("[ScoreHandler - UNPACK]: Sim Time Elapsed: %d %d", m_score.sim_time_elapsed.sec, 
             m_score.sim_time_elapsed.nsec);
    ROS_INFO("[ScoreHandler - UNPACK]: Completion Score %d", m_score.completion_score);
    ROS_INFO("[ScoreHandler - UNPACK]: Falls %d", m_score.falls);

    // Publish the data
    publish();
}


/**
 * Publish the most recent data on the appropriate ROS topic. This data
 * might be published once or published repeatedly via a timer.
 */
void ScoreHandler::publish()
{
    // Publish the data
    m_uiPub.publish(m_score);
}


/**
 * Send a request to the ReducedMsgController requesting data. This should
 * be a single char (the id) as an odd number. More info can be specified
 * if you wish to only get a subset of data.
 */
void ScoreHandler::requestData(const std_msgs::Bool& msg )
{
    m_rmc->send(m_handlerId, NULL, 0);
}

void ScoreHandler::convertSmallest()
{
}
void ScoreHandler::convertSmall()
{
}
void ScoreHandler::convertMedium()
{
}
void ScoreHandler::convertLarge()
{
}
void ScoreHandler::convertLargest()
{
}
