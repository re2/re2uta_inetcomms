/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <re2uta/JointCmdHandler.h>
#include <re2uta/ReducedMsgController.h>

using namespace re2uta;

/**
 * Constructor takes in a ros node and a pointer to the ReducedMsgController
 */
JointCmdHandler::JointCmdHandler( ros::NodeHandle node, ReducedMsgController *rmc )
{
    m_node = node;
    m_rmc = rmc;

    setup();
}

JointCmdHandler::~JointCmdHandler()
{
}

/**
 * Initialize the handler.
 */
void JointCmdHandler::setup()
{
    // Grab the params from the param server (set via the launch file)
    int tempHandlerId;
    m_node.param<int>( "/re2uta/JointCmdHandler_id", tempHandlerId, 0);

    // These are the joint commands published from the data handler to the simulator
    m_node.param<std::string>( "/re2uta/JointCmdHandler_rosTopicCmdToSim", m_rosTopicPubVal, "/atlas/atlas_command");

    // This is the joint command sent from the OCU to the data handler 
    m_node.param<std::string>( "/re2uta/JointCmdHandler_rosTopicCmdFromOCU",  m_rosTopicSubUIVal, "/ui/joint_cmds");

    m_handlerId = tempHandlerId;

    // Setup the publisher
    m_cmdPub = m_node.advertise<atlas_msgs::AtlasCommand>( m_rosTopicPubVal, 1 );
    // Subscribe to joint command data coming from the OCU
    m_cmdSub  = m_node.subscribe( m_rosTopicSubUIVal,  1, &JointCmdHandler::rcvJointCmdDataCB, this );

    // subscrive to joint states for initialization only
    m_jointStateSub = m_node.subscribe("/atlas/joint_states", 1, &JointCmdHandler::rcvJointStateDataCB, this);
    m_jointStateInitialized = false;

    // THIS MAY BE TEMPORARY DEPENDING ON WHAT GOES TO THE CONTROLLER
    m_jointCmdsToPublish.position.resize(28);
    m_jointCmdsToPublish.velocity.resize(28);
    m_jointCmdsToPublish.effort.resize(28);
    m_jointCmdsToPublish.kp_position.resize(28);
    m_jointCmdsToPublish.ki_position.resize(28);
    m_jointCmdsToPublish.kd_position.resize(28);
    m_jointCmdsToPublish.kp_velocity.resize(28);
    m_jointCmdsToPublish.i_effort_min.resize(28);
    m_jointCmdsToPublish.i_effort_max.resize(28);
    m_jointCmdsToPublish.k_effort.resize(28);
    
    for (int i=0; i<28; i++) {
        m_jointCmdsToPublish.position[i] = 0.0;
        m_jointCmdsToPublish.velocity[i] = 0.0;
        m_jointCmdsToPublish.effort[i] = 0.0;
        m_jointCmdsToPublish.ki_position[i] = 0.0;
        m_jointCmdsToPublish.kd_position[i] = 0.0;
        m_jointCmdsToPublish.kp_velocity[i] = 50.0;
        m_jointCmdsToPublish.i_effort_min[i] = 0.0;
        m_jointCmdsToPublish.i_effort_max[i] = 0.0;
        m_jointCmdsToPublish.k_effort[i] = 255;
    }

    // this gives control of the legs to the BDI controller
    for (int i=5; i<16; i++) {
        m_jointCmdsToPublish.k_effort[i] = 0;
    }
        
    // set some gains
    for (int i=0; i<4; i++) {
        m_jointCmdsToPublish.kp_position[i] = 4000;
    }
    for (int i=16; i<28; i++) {
        m_jointCmdsToPublish.kp_position[i] = 4000;
    }    
}

void JointCmdHandler::rcvJointStateDataCB( const sensor_msgs::JointState& msg )
{
    if (m_jointStateInitialized == false) {
        m_jointCmdsToPublish.position = msg.position;
        m_jointStateInitialized = true;
    }
}


/**
 * Receive the joint command data from the OCU, pack and send
 */
void JointCmdHandler::rcvJointCmdDataCB( const re2uta_inetComms::JointCommand& msg )
{
//    ROS_INFO( "[JointCmdHandler] Received a joint command from OCU" );

    m_jointCmdsToSend = msg;

    // pack and send it on its way
    pack(NULL);
}

/**
 * Uncompresses, hydrates, or otherwise unpacks the data (if applicable), then
 * publishes it on the appropriate ROS topic.
 */
void JointCmdHandler::unpack( unsigned char* data )
{
    unsigned int numCmds;
    int msgIndex = 1;
    int jointId;
    unsigned short temp;
    double dtemp;

//    ROS_INFO( "Extracting JointCmd joint cmd data" );

    numCmds = (unsigned int) data[0];

    for (int i=0; i<(3*numCmds+1); i++) {
      ROS_INFO("[JointCmdHandler]: RCV data[%d] = %d", i, data[i]);
    }

    for (unsigned int i=0; i<numCmds; i++) {
        jointId = data[msgIndex];

//        ROS_INFO("JOINT ID = %d", data[msgIndex]);

        msgIndex++;

//        ROS_INFO("UNPACK: %d %d %d", msgIndex, data[msgIndex], data[msgIndex+1]);

        temp = *((unsigned short *)& (data[msgIndex]));
        dtemp = (double) temp;
//        ROS_INFO("UNPACK: %d %d %g", jointId, temp, dtemp);
        m_jointCmdsToPublish.position[jointId] = dtemp * 1e-4 - M_PI;
    ROS_INFO("[JointCmdHandler]: UNPACK: %d = %g", jointId, m_jointCmdsToPublish.position[jointId] * 57.295791);
        msgIndex += 2;
    }

    // Publish the data
    publish();
}

/**
 * Compress, dilute, or otherwise reduce the data, then hand it off to the
 * ReducedMsgController to be sent across the line/internet.
 */
void JointCmdHandler::pack( char* params )
{
    char bufOut[85];   // 85 is the maximum number of bytes - could be much less
    int numCmds = 0;
    int msgIndex = 1;
    unsigned short temp;

//    ROS_INFO( "[JointCmdHandler] Packing joint cmd data" );

    // store any valid commands in the command message
    for (int i=0; i<28; i++) {
        if (m_jointCmdsToSend.valid[i] == 1) {
      
            numCmds++;

            // store the joint id
            bufOut[msgIndex] = (char) i;
            msgIndex++;

            // store the joint position
            temp = (unsigned short) (10000 * (m_jointCmdsToSend.cmd.position[i] + M_PI)); 
//            ROS_INFO("PACK: %g = %d = %d", m_jointCmdsToSend.cmd.position[i], i, temp);
            *((unsigned short *)& (bufOut[msgIndex])) = temp;
            msgIndex += 2;
        }
    }
    
    // save the number of commands
    bufOut[0] = (char) numCmds;

//    m_rmc->send((m_handlerId + 1), bufOut, (3*numCmds+1));
    m_rmc->queueMsg((m_handlerId + 1), bufOut, (3*numCmds+1), "Joint Command");
}

/**
 * Publish the most recent data on the appropriate ROS topic. This data
 * might be published once or published repeatedly via a timer.
 */
void JointCmdHandler::publish()
{
    // Publish the data
    m_cmdPub.publish(m_jointCmdsToPublish);
}

void JointCmdHandler::convertSmallest()
{
}

void JointCmdHandler::convertSmall()
{
}

void JointCmdHandler::convertMedium()
{
}

void JointCmdHandler::convertLarge()
{
}

void JointCmdHandler::convertLargest()
{
}

