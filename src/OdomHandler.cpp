/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#include <re2uta/OdomHandler.h>
#include <re2uta/ReducedMsgController.h>

#include <std_msgs/Int8.h>

#include <tf/LinearMath/Vector3.h>
#include <tf/LinearMath/Quaternion.h>

using namespace re2uta;

//static const uint8_t RE2UTA_REQ_ODOM = 1;
static const uint8_t RE2UTA_REQ_VIZ_ODOM = 2;
static const uint8_t RE2UTA_REQ_KIN_ODOM = 3;

/**
 * Constructor takes in a ros node and a pointer to the ReducedMsgController
 */
OdomHandler::OdomHandler( ros::NodeHandle node, ReducedMsgController *rmc )
{
    m_node = node;
    m_rmc = rmc;

    setup();
}

OdomHandler::~OdomHandler()
{
}

/**
 * Initialize the handler.
 */
void OdomHandler::setup()
{
    // Grab the params from the param server (set via the launch file)
    bool client;
    int tempHandlerId;
    m_node.param<int>(         "/re2uta/OdomHandler_id",            tempHandlerId,      0              );
    m_node.param<std::string>( "/re2uta/OdomHandler_rosTopicUIReq", m_rosTopicSubUIVal, "/ui/reqOdom"  );
    m_node.param<bool>(        "/re2uta/RMC_client",                client,             false          );
    m_node.param<std::string>( "/re2uta/OdomHandler_odomTfName",    m_odomTfName,       "/odom"        );
    m_node.param<std::string>( "/re2uta/OdomHandler_visOdomTfName", m_visOdomTfName,    "/visual_odom" );
    m_node.param<std::string>( "/re2uta/OdomHandler_kinOdomTfName", m_kinOdomTfName,    "/kin_odom"    );
    m_node.param<std::string>( "/re2uta/OdomHandler_parentFrame",   m_parentFrame,      "/pelvis"      );

    m_handlerId = tempHandlerId;

    setOdomChoice(RE2UTA_REQ_KIN_ODOM);

    // Setup the publisher and subscriber
    m_uiSub = m_node.subscribe( m_rosTopicSubUIVal, 1, &OdomHandler::rcvReqOdomCB, this );

//    m_tfListener.setExtrapolationLimit(ros::Duration(0.1));

    // Add timer to constantly publish the current TF data
    if(client)
    {
        m_pubTimer      = m_node.createTimer(ros::Duration(0,10), &OdomHandler::pubTf,    this);
    }
    else
    {
        m_updateTfTimer = m_node.createTimer(ros::Duration(0,10), &OdomHandler::updateTf, this);
    }
}

void OdomHandler::setOdomChoice(int newChoice)
{
    m_odomChoice = newChoice;

//    // Point to the current odom choice
//    switch(m_odomChoice)
//    {
//    case RE2UTA_REQ_VIZ_ODOM:
//        m_chosenTransform = &m_currentVizOdom;
//        break;
//    case RE2UTA_REQ_KIN_ODOM:
//        m_chosenTransform = &m_currentKinOdom;
//        break;
//    default:
//        m_chosenTransform = &m_currentKinOdom;
//        break;
//    }
}

/**
 * Uncompresses, hydrates, or otherwise unpacks the data (if applicable), then
 * publishes it on the appropriate ROS topic.
 */
void OdomHandler::unpack( unsigned char* data )
{
    int paramsIndex = 0;
    char baseOneTwentySevenNumber[3];
    float positionX    = 0.0;
    float positionY    = 0.0;
    float positionZ    = 0.0;
    float orientationX = 0.0;
    float orientationY = 0.0;
    float orientationZ = 0.0;
    float orientationW = 0.0;

//    for (int i=0; i<21; i++)
//    {
//      ROS_INFO("[ODOM HANDLER - RECV] buf[%d] = %d", i, data[i]);
//    }

    // POSE
    // x
    baseOneTwentySevenNumber[0] = data[paramsIndex]; // 0
    paramsIndex++;
    baseOneTwentySevenNumber[1] = data[paramsIndex]; // 1
    paramsIndex++;
    baseOneTwentySevenNumber[2] = data[paramsIndex]; // 2
    paramsIndex++;
    positionX = toBaseTen(baseOneTwentySevenNumber);

    // y
    baseOneTwentySevenNumber[0] = data[paramsIndex]; // 3
    paramsIndex++;
    baseOneTwentySevenNumber[1] = data[paramsIndex]; // 4
    paramsIndex++;
    baseOneTwentySevenNumber[2] = data[paramsIndex]; // 5
    paramsIndex++;
    positionY = toBaseTen(baseOneTwentySevenNumber);

    // z
    baseOneTwentySevenNumber[0] = data[paramsIndex]; // 6
    paramsIndex++;
    baseOneTwentySevenNumber[1] = data[paramsIndex]; // 7
    paramsIndex++;
    baseOneTwentySevenNumber[2] = data[paramsIndex]; // 8
    paramsIndex++;
    positionZ = toBaseTen(baseOneTwentySevenNumber);

    // ORIENTATION
    // x
    baseOneTwentySevenNumber[0] = data[paramsIndex]; // 9
    paramsIndex++;
    baseOneTwentySevenNumber[1] = data[paramsIndex]; // 10
    paramsIndex++;
    baseOneTwentySevenNumber[2] = data[paramsIndex]; // 11
    paramsIndex++;
    orientationX = toBaseTen(baseOneTwentySevenNumber);

    // y
    baseOneTwentySevenNumber[0] = data[paramsIndex]; // 12
    paramsIndex++;
    baseOneTwentySevenNumber[1] = data[paramsIndex]; // 13
    paramsIndex++;
    baseOneTwentySevenNumber[2] = data[paramsIndex]; // 14
    paramsIndex++;
    orientationY = toBaseTen(baseOneTwentySevenNumber);

    // z
    baseOneTwentySevenNumber[0] = data[paramsIndex]; // 15
    paramsIndex++;
    baseOneTwentySevenNumber[1] = data[paramsIndex]; // 16
    paramsIndex++;
    baseOneTwentySevenNumber[2] = data[paramsIndex]; // 17
    paramsIndex++;
    orientationZ = toBaseTen(baseOneTwentySevenNumber);

    //w
    baseOneTwentySevenNumber[0] = data[paramsIndex]; // 18
    paramsIndex++;
    baseOneTwentySevenNumber[1] = data[paramsIndex]; // 19
    paramsIndex++;
    baseOneTwentySevenNumber[2] = data[paramsIndex]; // 20
    paramsIndex++;
    orientationW = toBaseTen(baseOneTwentySevenNumber);


    ROS_INFO("[OdomHandler]   Pose:");
    ROS_INFO("[OdomHandler]     x: %f", positionX);
    ROS_INFO("[OdomHandler]     y: %f", positionY);
    ROS_INFO("[OdomHandler]     z: %f", positionZ);
    ROS_INFO("[OdomHandler]   Orientation:");
    ROS_INFO("[OdomHandler]     x: %f", orientationX);
    ROS_INFO("[OdomHandler]     y: %f", orientationY);
    ROS_INFO("[OdomHandler]     z: %f", orientationZ);
    ROS_INFO("[OdomHandler]     w: %f", orientationW);

    // Update the variables that will eventually be published on the tf tree
    tf::Vector3 myOrigin(positionX, positionY, positionZ);
    tf::Quaternion myRotation(orientationX, orientationY, orientationZ, orientationW);

    m_currentOdom.setOrigin(myOrigin);
    m_currentOdom.setRotation(myRotation);
    m_currentOdom.frame_id_ = m_parentFrame;
    m_currentOdom.child_frame_id_ = m_odomTfName;
    m_currentOdom.stamp_ = ros::Time::now();
//    ROS_INFO("[OdomHandler] Publishing %s on /tf",m_currentOdom.child_frame_id_.c_str());

    switch(m_odomChoice)
    {
    case RE2UTA_REQ_VIZ_ODOM:
        m_currentVizOdom.setOrigin(myOrigin);
        m_currentVizOdom.setRotation(myRotation);
        m_currentVizOdom.frame_id_ = m_parentFrame;
        m_currentVizOdom.child_frame_id_ = m_visOdomTfName;
        m_currentVizOdom.stamp_ = ros::Time::now();
//        ROS_INFO("[OdomHandler] Publishing %s on /tf",m_currentVizOdom.child_frame_id_.c_str());
        break;
    case RE2UTA_REQ_KIN_ODOM:
        m_currentKinOdom.setOrigin(myOrigin);
        m_currentKinOdom.setRotation(myRotation);
        m_currentKinOdom.frame_id_ = m_parentFrame;
        m_currentKinOdom.child_frame_id_ = m_kinOdomTfName;
        m_currentKinOdom.stamp_ = ros::Time::now();
//        ROS_INFO("[OdomHandler] Publishing %s on /tf",m_currentKinOdom.child_frame_id_.c_str());
        break;
    default:
        m_currentKinOdom.setOrigin(myOrigin);
        m_currentKinOdom.setRotation(myRotation);
        m_currentKinOdom.frame_id_ = m_parentFrame;
        m_currentKinOdom.child_frame_id_ = m_kinOdomTfName;
        m_currentKinOdom.stamp_ = ros::Time::now();
//        ROS_INFO("[OdomHandler] (Default) Publishing %s on /tf",m_currentKinOdom.child_frame_id_.c_str());
        break;
    }
}

/**
 * Compress, dilute, or otherwise reduce the data, then hand it off to the
 * ReducedMsgController to be sent across the line/internet.
 */
void OdomHandler::pack( char* params )
{
    float poseX;
    float poseY;
    float poseZ;
    float orienX;
    float orienY;
    float orienZ;
    float orienW;

    int dataOutSize = (3*3) + (4*3); // Pose(3) * 3 chars each + orientation(4) * 3 chars each
    char dataOut[dataOutSize];

    char baseOneTwentySevenNumber[3];

    setOdomChoice(params[0]);

    // Grab the appropriate tf (odom/viz odom/kin odom)
    switch(m_odomChoice)
    {
//    case RE2UTA_REQ_ODOM:
//        poseX  = m_currentOdom.getOrigin().getX();
//        poseY  = m_currentOdom.getOrigin().getY();
//        poseZ  = m_currentOdom.getOrigin().getZ();
//        orienX = m_currentOdom.getRotation().getX();
//        orienY = m_currentOdom.getRotation().getY();
//        orienZ = m_currentOdom.getRotation().getZ();
//        orienW = m_currentOdom.getRotation().getW();
//        break;
    case RE2UTA_REQ_VIZ_ODOM:
        poseX  = m_currentVizOdom.getOrigin().getX();
        poseY  = m_currentVizOdom.getOrigin().getY();
        poseZ  = m_currentVizOdom.getOrigin().getZ();
        orienX = m_currentVizOdom.getRotation().getX();
        orienY = m_currentVizOdom.getRotation().getY();
        orienZ = m_currentVizOdom.getRotation().getZ();
        orienW = m_currentVizOdom.getRotation().getW();
        break;
    case RE2UTA_REQ_KIN_ODOM:
        poseX  = m_currentKinOdom.getOrigin().getX();
        poseY  = m_currentKinOdom.getOrigin().getY();
        poseZ  = m_currentKinOdom.getOrigin().getZ();
        orienX = m_currentKinOdom.getRotation().getX();
        orienY = m_currentKinOdom.getRotation().getY();
        orienZ = m_currentKinOdom.getRotation().getZ();
        orienW = m_currentKinOdom.getRotation().getW();
        break;
    default:
        poseX  = m_currentKinOdom.getOrigin().getX();
        poseY  = m_currentKinOdom.getOrigin().getY();
        poseZ  = m_currentKinOdom.getOrigin().getZ();
        orienX = m_currentKinOdom.getRotation().getX();
        orienY = m_currentKinOdom.getRotation().getY();
        orienZ = m_currentKinOdom.getRotation().getZ();
        orienW = m_currentKinOdom.getRotation().getW();
        break;
    }

    ///////////////////////////////////

    ROS_INFO("[ODOM HANDLER - PACK] Pose = (%g %g %g) (%g %g %g %g)",
         poseX, poseY, poseZ, orienX, orienY, orienZ, orienW);

    if(isnan(poseX))
    {
        ROS_WARN("[OdomHandler] pose x was NAN. Using 0 as its value.");
        poseX = 0.0;
    }
    if(isnan(poseY))
    {
        ROS_WARN("[OdomHandler] pose y was NAN. Using 0 as its value.");
        poseY = 0.0;
    }
    if(isnan(poseZ))
    {
        ROS_WARN("[OdomHandler] pose z was NAN. Using 0 as its value.");
        poseZ = 0.0;
    }
    if(isnan(orienX))
    {
        ROS_WARN("[OdomHandler] orientation x was NAN. Using 0 as its value.");
        orienX = 0.0;
    }
    if(isnan(orienY))
    {
        ROS_WARN("[OdomHandler] orientation y was NAN. Using 0 as its value.");
        orienY = 0.0;
    }
    if(isnan(orienZ))
    {
        ROS_WARN("[OdomHandler] orientation z was NAN. Using 0 as its value.");
        orienZ = 0.0;
    }
    if(isnan(orienW))
    {
        ROS_WARN("[OdomHandler] orientation w was NAN. Using 0 as its value.");
        orienW = 0.0;
    }

    int bufIndex = 0;

    // POSE
    // x
    toBaseOneTwentySeven(poseX, baseOneTwentySevenNumber);
    dataOut[bufIndex] = baseOneTwentySevenNumber[0];
    bufIndex++;
    dataOut[bufIndex] = baseOneTwentySevenNumber[1];
    bufIndex++;
    dataOut[bufIndex] = baseOneTwentySevenNumber[2];
    bufIndex++;

    // y
    toBaseOneTwentySeven(poseY, baseOneTwentySevenNumber);
    dataOut[bufIndex] = baseOneTwentySevenNumber[0];
    bufIndex++;
    dataOut[bufIndex] = baseOneTwentySevenNumber[1];
    bufIndex++;
    dataOut[bufIndex] = baseOneTwentySevenNumber[2];
    bufIndex++;

    // z is always 0
    toBaseOneTwentySeven(poseZ, baseOneTwentySevenNumber);
    dataOut[bufIndex] = baseOneTwentySevenNumber[0];
    bufIndex++;
    dataOut[bufIndex] = baseOneTwentySevenNumber[1];
    bufIndex++;
    dataOut[bufIndex] = baseOneTwentySevenNumber[2];
    bufIndex++;

    // ORIENTATION
    // x
    toBaseOneTwentySeven(orienX, baseOneTwentySevenNumber);
    dataOut[bufIndex] = baseOneTwentySevenNumber[0];
    bufIndex++;
    dataOut[bufIndex] = baseOneTwentySevenNumber[1];
    bufIndex++;
    dataOut[bufIndex] = baseOneTwentySevenNumber[2];
    bufIndex++;

    // y
    toBaseOneTwentySeven(orienY, baseOneTwentySevenNumber);
    dataOut[bufIndex] = baseOneTwentySevenNumber[0];
    bufIndex++;
    dataOut[bufIndex] = baseOneTwentySevenNumber[1];
    bufIndex++;
    dataOut[bufIndex] = baseOneTwentySevenNumber[2];
    bufIndex++;

    // z
    toBaseOneTwentySeven(orienZ, baseOneTwentySevenNumber);
    dataOut[bufIndex] = baseOneTwentySevenNumber[0];
    bufIndex++;
    dataOut[bufIndex] = baseOneTwentySevenNumber[1];
    bufIndex++;
    dataOut[bufIndex] = baseOneTwentySevenNumber[2];
    bufIndex++;

    // w
    toBaseOneTwentySeven(orienW, baseOneTwentySevenNumber);
    dataOut[bufIndex] = baseOneTwentySevenNumber[0];
    bufIndex++;
    dataOut[bufIndex] = baseOneTwentySevenNumber[1];
    bufIndex++;
    dataOut[bufIndex] = baseOneTwentySevenNumber[2];
    bufIndex++;

    //    ROS_INFO( "[OdomHandler] Data packed. Sending TF data!" );

//    for (int i=0; i<dataOutSize; i++)
//    {
//      ROS_INFO("[ODOM HANDLER - SEND] buf[%d] = %d", i, dataOut[i]);
//    }

    m_rmc->send( (m_handlerId + 1), dataOut, dataOutSize );
}

/**
 *
 */
void OdomHandler::toBaseOneTwentySeven(float baseTenNum, char* baseOneTwentySevenNumber)
{
    int baseTenDecimalShifted;
    int baseOneTwentySevenDigit;

    baseTenDecimalShifted = (int)((baseTenNum*(pow(10,ODOM_NUM_DIGITS_TO_KEEP)))+0.00003); // +0.000001 because computers are stupid

    for(int exponent=0; exponent<3; ++exponent) // Always have three digits per value
    {
        baseOneTwentySevenDigit = (( (int)( baseTenDecimalShifted/(pow(ODOM_BASE,exponent)) ) ) % ODOM_BASE);

        baseOneTwentySevenNumber[2-exponent] = (char)baseOneTwentySevenDigit;
    }
}

/**
 *
 */
float OdomHandler::toBaseTen(char* baseOneTwentySevenNumber)
{
    float baseTenNumber = 0;
    int digitIndex = 0;
    int baseOneTwentySevenDigit;

    for(int exponent=2; exponent>=0; --exponent) // Always have three digits per value
    {
        baseOneTwentySevenDigit = (int)(baseOneTwentySevenNumber[digitIndex]);
        ++digitIndex;

        baseTenNumber += ((baseOneTwentySevenDigit ) * pow(ODOM_BASE,exponent))*pow(10, -1*ODOM_NUM_DIGITS_TO_KEEP);
    }

    return baseTenNumber;
}

/**
 */
void OdomHandler::convertSmallest(){}

/**
 */
void OdomHandler::convertSmall(){}

/**
 */
void OdomHandler::convertMedium(){}

/**
 */
void OdomHandler::convertLarge(){}

/**
 */
void OdomHandler::convertLargest(){}

/**
 * Send a request to the ReducedMsgController requesting data. This should
 * be a single char (the id) as an odd number. More info can be specified
 * if you wish to only get a subset of data.
 */
void OdomHandler::rcvReqOdomCB( const std_msgs::Int8ConstPtr& msg )
{
    ROS_INFO( "[OdomHandler] Received request for Odom info. Sending req to RMC." );

    int bufSize = 1;
    char bufWithParams[bufSize];

    setOdomChoice(msg->data);

    bufWithParams[0] = (char)m_odomChoice;

    // Send buf
    m_rmc->send( m_handlerId, bufWithParams, bufSize );
}

void OdomHandler::publish()
{
    // Not needed
}

/**
 * Grab the tf on the robot side
 */
void OdomHandler::updateTf(const ros::TimerEvent& e)
{
//    ROS_INFO("[OdomHandler] Updating tf");
    try{
        // NOTE: Not sure what argument goes first...
        listener.lookupTransform(m_parentFrame, m_visOdomTfName,
                ros::Time(0), m_currentVizOdom); // Time 0 = latest
        ROS_INFO_ONCE("[OdomHandler] Receiving visual odom");
    }
    catch (tf::TransformException ex){
//        ROS_WARN_THROTTLE(10,"[OdomHandler] Unable to look up transform from %s to %s: %s", m_visOdomTfName.c_str(), m_parentFrame.c_str(), ex.what());
        ROS_WARN_ONCE("[OdomHandler] Vis Odom - Unable to look up transform from %s to %s", m_visOdomTfName.c_str(), m_parentFrame.c_str());
    }

    try{
        // NOTE: Not sure what argument goes first...
        // NOTE: Kin odom is actually published on /odom on the robot side.
//        listener.lookupTransform(m_kinOdomTfName, m_parentFrame,
        listener.lookupTransform(m_parentFrame, m_odomTfName,
                ros::Time(0), m_currentKinOdom); // Time 0 = latest
        ROS_INFO_ONCE("[OdomHandler] Receiving kinematic odom");
    }
    catch (tf::TransformException ex){
//        ROS_WARN_THROTTLE(10,"[OdomHandler] Unable to look up transform from %s to %s: %s", m_kinOdomTfName.c_str(), m_parentFrame.c_str(), ex.what());
        ROS_WARN_THROTTLE(3,"[OdomHandler] Kin odom - Unable to look up transform from %s to %s", m_odomTfName.c_str(), m_parentFrame.c_str());
    }
}

/**
 * Publish the tf on the ocu side
 */
void OdomHandler::pubTf(const ros::TimerEvent& e)
{
//    ROS_INFO("[OdomHandler] Publishing tf");

    // Current odom
    if(!m_currentOdom.frame_id_.empty())
    {
        ROS_INFO_ONCE("[OdomHandler] Publishing %s on /tf",m_currentOdom.child_frame_id_.c_str());
        m_currentOdom.stamp_ = ros::Time::now();
        m_tfBroadcaster.sendTransform( m_currentOdom );
    }
    else
    {
        ROS_WARN_ONCE("[OdomHandler] You need to request an odom before it can be published");
    }

    // Vis odom
    if(!m_currentVizOdom.frame_id_.empty())
    {
        ROS_INFO_ONCE("[OdomHandler] Publishing %s on /tf",m_currentVizOdom.child_frame_id_.c_str());
        m_currentVizOdom.stamp_ = ros::Time::now();
        m_tfBroadcaster.sendTransform( m_currentVizOdom );
    }
    else
    {
        ROS_WARN_ONCE("[OdomHandler] You need to request vis odom before it can be published");
    }

    // Kin odom
    if(!m_currentKinOdom.frame_id_.empty())
    {
        ROS_INFO_ONCE("[OdomHandler] Publishing %s on /tf",m_currentKinOdom.child_frame_id_.c_str());
        m_currentKinOdom.stamp_ = ros::Time::now();
        m_tfBroadcaster.sendTransform( m_currentKinOdom );
    }
    else
    {
        ROS_WARN_ONCE("[OdomHandler] You need to request kin odom before it can be published");
    }
}
