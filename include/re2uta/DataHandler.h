/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#ifndef DATAHANDLER_H_
#define DATAHANDLER_H_

#include <string>
#include <ros/ros.h>

#include <re2uta_inetComms/RequestData.h>

namespace re2uta
{

class ReducedMsgController;

static const int UNKNOWN = 0;

static const int COMM_LVL_SMALLEST = 1;
static const int COMM_LVL_SMALL    = 2;
static const int COMM_LVL_MEDIUM   = 3;
static const int COMM_LVL_LARGE    = 4;
static const int COMM_LVL_LARGEST  = 5;

/**
 * This class contains virtual functions that are implemented by the specific
 * data handlers (e.g. the JointStateHandler). Data Handlers handle the the
 * compression, dilution, or other massaging of the data prior to being sent over
 * the line. The handlers also unpack the data, if applicable, that is received
 * from the other computer, and transmits the data to the appropriate ROS topic.
 */
class DataHandler
{

public:
    virtual ~DataHandler()
    {
    }

    void setCommLvl( int newCommLvl )
    {
        m_commLvl = newCommLvl;
    };

    unsigned char getHandlerId()
    {
        return m_handlerId;
    };

    /**
     * Uncompresses, hydrates, or otherwise unpacks the data (if applicable), then
     * publishes it on the appropriate ROS topic.
     */
    virtual void unpack( unsigned char* data ) = 0;

    /**
     * Compress, dilute, or otherwise reduce the data, then hand it off to the
     * ReducedMsgController to be sent across the line/internet.
     */
    virtual void pack( char* params ) = 0;

protected:
    // The level of detail the data should be compressed. Note, this may differ
    //  from the current bit scenario - in some cases you may wish to get a more
    //  detailed picture despite the low bit scenario.
    int m_commLvl;

    // The ROS topics that talk to the UI/robot
    std::string m_rosTopicPubVal;
    std::string m_rosTopicSubUIVal; // Subscribe to UI messages
    std::string m_rosTopicSubDataVal; // Subscribe to Data messages

    // The handlerId is a unique id that is added to messages going over the
    //  connection to identify what type of message or request is being sent.
    //  Odd values = request for data
    //  Even = data msg
    // Example: id of 3 might be a request for point cloud data, 4 would be a
    //  msg containing point cloud data.
    unsigned char m_handlerId;

    ros::NodeHandle m_node;

    ReducedMsgController *m_rmc;

private:
    virtual void setup() = 0;

    /**
     * Publish the most recent data on the appropriate ROS topic. This data
     * might be published once or published repeatedly via a timer.
     */
    virtual void publish() = 0;

    /**
     * Receive the data from a ros topic (a callback function) and store it.
     * Note, just because we receive data does not mean we always want to send
     * that data over the line. Store the data until a request is made for that
     * data to be sent over the line or, if a timer is setup, when the timer
     * expires signifying the data should be sent.
     *
     * Note: This will be different for each type of data handler.
     */
    //    void rcvDataCB(YOUR_MSG_TYPE);
//    /**
//     * Send a request to the ReducedMsgController requesting data. This should
//     * be a single char (the id) as an odd number. More info can be specified
//     * if you wish to only get a subset of data.
//     */
//    virtual void requestData( const re2uta_inetComms::RequestData& msg ) = 0;

    virtual void convertSmallest() = 0;
    virtual void convertSmall() = 0;
    virtual void convertMedium() = 0;
    virtual void convertLarge() = 0;
    virtual void convertLargest() = 0;

};

} // End namespace

#endif /* DATAHANDLER_H_ */
