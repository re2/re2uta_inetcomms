/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#ifndef WALKINGHANDLER_H_
#define WALKINGHANDLER_H_

#include <re2uta/DataHandler.h>

#include <geometry_msgs/PoseStamped.h>

#include <re2uta_inetComms/WalkingInfo.h>
#include <std_msgs/Bool.h>

#include <re2uta/walking/StepGenerator.hpp>
#include <re2uta/walking/WalkPatternGenerator.hpp>
#include <re2uta/walking/CirclePathStepGenerator.hpp>
#include <re2/visutils/marker_util.h>
#include <eigen_conversions/eigen_msg.h>
#include <nav_msgs/Path.h>
#include <geometry_msgs/PoseStamped.h>
#include <ros/ros.h>
#include <boost/foreach.hpp>
#include <string>
#include <iostream>
#include <queue>
#include <vector>

#include <std_msgs/String.h>
#include <std_msgs/Empty.h>
#include <re2uta_inetComms/QueueInfo.h>
#include <re2uta_atlasCommander/WalkPlan.h>
#include <tf/transform_listener.h>

namespace re2uta
{

static const int WALKING_NUM_DIGITS_TO_KEEP = 4;
static const int WALKING_BASE = 127;

class WalkingHandler : public DataHandler
{

public:
    WalkingHandler( ros::NodeHandle node, ReducedMsgController *rmc );
    ~WalkingHandler();

    void unpack( unsigned char* data );

private:
    ros::Subscriber m_uiSub;
    ros::Publisher  m_uiPub;
    re2uta_atlasCommander::WalkPlan m_poseMsgs;
    geometry_msgs::PoseStamped m_latestPoseMsg;
    ros::Subscriber m_uiQueuePathSub;
    ros::Subscriber m_msgQueueSub;
    ros::Publisher m_msgQueuePub;
    int m_odomId;

    std::vector<geometry_msgs::PoseStamped> m_queuedMsgs;

    ros::Publisher  m_walkCommanderSelPub;
    ros::Publisher  m_stepGeneratorSelPub;
    ros::Publisher  m_jointControllerSelPub;
    ros::Subscriber m_walkCommanderChangedSub;

    std::string m_currentWalkCommander;
    std::string m_currentStepGenerator;
    std::string m_currentJointController;
    std::string m_currentFoot;

    tf::TransformListener m_listener;

    void setup();

    void pack( char* params );
    void publish();
    void requestData( const geometry_msgs::PoseStampedConstPtr & msg );
    void queuePathCB( const re2uta_inetComms::WalkingInfoConstPtr & msg );

    void convertSmallest();
    void convertSmall();
    void convertMedium();
    void convertLarge();
    void convertLargest();

    void toBaseOneTwentySeven(float baseTenNum, char* baseOneTwentySevenNumber);
    float toBaseTen(char* baseOneTwentySevenNumber);

    void sendToRobot(bool queue);

    re2uta::WalkPatternGenerator m_walkPatternGenerator;
    Eigen::Affine3d m_dest;

    ros::Publisher lFootTrajPub;
    ros::Publisher rFootTrajPub;
    ros::Publisher lFootPosePub;
    ros::Publisher rFootPosePub;
    ros::Publisher comTrajPub  ;
    ros::Publisher simMarkerPub;
    ros::Publisher m_footstepMarkerPub;

    void makeCirclePath();
    void makeNaoPath();
    void makeStepPath(bool useCurrent);

    bool m_walkCommanderChanged;
    void walkCommanderChangedCB(const std_msgs::EmptyConstPtr & msg);

    int setParams(char *dataOut);
    int addPose(char* dataOut, int bufIndex, geometry_msgs::PoseStamped poseMsg);
    int createPoseFromBuf(char *params, int paramsIndex);
};

} // End namespace

#endif /* WALKINGHANDLER_H_ */
