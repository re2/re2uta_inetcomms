/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#ifndef LEFTHANDSTATEHANDLER_H_
#define LEFTHANDSTATEHANDLER_H_

#include <re2uta/DataHandler.h>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Empty.h>

namespace re2uta
{

class LeftHandStateHandler : public DataHandler
{
public:
    LeftHandStateHandler( ros::NodeHandle node, ReducedMsgController *rmc );
    ~LeftHandStateHandler();

    void unpack( unsigned char* data );

private:
    ros::Publisher m_uiPub;
    ros::Subscriber m_jsSub;
    ros::Subscriber m_uiSub;

    unsigned short m_packedLeftHandState[12];
    sensor_msgs::JointState m_rosLeftHandState;

    void rcvJSDataCB( const sensor_msgs::JointState& msg );

    void setup();

    void pack( char* params );
    void publish();
    void requestData(const std_msgs::Empty& msg);

    void convertSmallest();
    void convertSmall();
    void convertMedium();
    void convertLarge();
    void convertLargest();
};

} // End namespace

#endif /* LEFTHANDSTATEHANDLER_H_ */
