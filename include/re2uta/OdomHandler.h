/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#ifndef ODOMHANDLER_H_
#define ODOMHANDLER_H_

#include <re2uta/DataHandler.h>

#include <std_msgs/Int8.h>

#include <tf/transform_listener.h>
#include <tf/transform_broadcaster.h>

namespace re2uta
{

static const int ODOM_NUM_DIGITS_TO_KEEP = 4;
static const int ODOM_BASE = 127;

class OdomHandler : public DataHandler
{

public:
    OdomHandler( ros::NodeHandle node, ReducedMsgController *rmc );
    ~OdomHandler();

    void unpack( unsigned char* data );

private:
    ros::Subscriber m_uiSub;

    ros::Timer m_updateTfTimer;
    ros::Timer m_pubTimer;
    tf::TransformListener listener;
    tf::TransformBroadcaster m_tfBroadcaster;
    std::string m_odomTfName;
    std::string m_visOdomTfName;
    std::string m_kinOdomTfName;
    std::string m_parentFrame;

//    tf::StampedTransform transform;
    tf::StampedTransform m_currentOdom;
    tf::StampedTransform m_currentVizOdom;
    tf::StampedTransform m_currentKinOdom;
//    tf::StampedTransform m_currentTransform;
//    tf::StampedTransform *m_chosenTransform;

    int m_odomChoice;

    void rcvReqOdomCB( const std_msgs::Int8ConstPtr& msg );

    void setup();

    void pack( char* params );
    void publish();

    void convertSmallest();
    void convertSmall();
    void convertMedium();
    void convertLarge();
    void convertLargest();

    void toBaseOneTwentySeven(float baseTenNum, char* baseOneTwentySevenNumber);
    float toBaseTen(char* baseOneTwentySevenNumber);

    void updateTf(const ros::TimerEvent& e);
    void pubTf(const ros::TimerEvent& e);
    void setOdomChoice(int newChoice);
};

} // End namespace

#endif /* ODOMHANDLER_H_ */
