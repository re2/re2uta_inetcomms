/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#ifndef REDUCEDMSGCONTROLLER_H_
#define REDUCEDMSGCONTROLLER_H_

#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>

#include <ros/ros.h>

#include <vector>
#include <queue>

#include <boost/shared_ptr.hpp>

//#include <re2uta/GhostAtlasPlugin.h>
#include <re2uta/DataHandler.h>

#include <re2uta_inetComms/QueueInfo.h>

#define MAX_BUF 1024
#define MAX_LARGE_MSG (255*MAX_BUF)

#define REQ_MSG_QUEUE         0
#define EXECUTE_MSG_QUEUE     1
#define REMOVE_FROM_MSG_QUEUE 2
#define CLEAR_MSG_QUEUE       3
#define THIS_IS_MSG_QUEUE     4

namespace re2uta
{

/**
 * This class will establish the connection between the OCU computer and Field
 * computer. It is responsible for sending and receiving data between the two
 * computers and monitoring the amount of data that has been exchanged.
 */
class ReducedMsgController
{

public:
    ReducedMsgController( ros::NodeHandle node );
    ~ReducedMsgController();

    void setBitScenario( int newBitScenario );
    DataHandler* getDataHandler( char id );

    void getMessageStats( int *numMsgsRcvd, int *numBitsRcvd, int *numMsgsSent, int *numBitsSent );

    void queueMsg( char msgId, char* bufOut, unsigned int length, std::string description );
    bool send( char msgId, char* bufOut, unsigned int length );

    bool rcv();

    void setNumBitsSent(int n);
    void setNumBitsRcvd(int n);

private:
    ros::NodeHandle m_node;
    ros::Publisher m_dataUpPub;
    ros::Publisher m_dataDownPub;
    ros::Publisher m_msgQueuePub;
    ros::Subscriber m_msgQueueSub;

    // UDP message stuff
    bool m_client; // is this RMC a server or client
    int m_socket;
    struct sockaddr_in m_myAddr;
    struct sockaddr_in m_serverAddr;
    struct sockaddr_in m_clientAddr;
    socklen_t m_addrlen;
    int m_timeout;

    struct queuedMsg
    {
        char msgId;
        char* bufOut;
        unsigned int length;
        std::string description;
    };

    std::queue<queuedMsg> m_queuedMsgs;

    // for large messages
    int m_largeMsgID;
    int m_largeMsgPacketCounter[256];
    unsigned char m_largeMsgBuffer[MAX_LARGE_MSG];

    // message and data statistics;
    int m_numMsgsRecvd;
    int m_numBitsRecvd;
    int m_numMsgsSent;
    int m_numBitsSent;

    int m_bitScenario; // One of five bit scenarios defined by DARPA
    std::vector<boost::shared_ptr<DataHandler> > m_dataHandlers; // The Data Handlers, such as ImgHandler, and JointStatesHandler

//    GhostAtlasPlugin m_ghostAtlas;

    void setup();
    bool createConnection( int client, std::string ipAddressP, int port );
    int isUDPDataThere( int *error );

    bool sendQueuedMsgs();
    void removeQueuedMsg(int queueIndex);
    void clearQueuedMsgs();
    void publishMsgQueue();
    void handleMsgQueueAction(const re2uta_inetComms::QueueInfoConstPtr& msg);
};

} // End namespace

#endif /* REDUCEDMSGCONTROLLER_H_ */
