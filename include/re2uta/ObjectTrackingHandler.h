/*
* COPYRIGHT (C) 2005-2013
* RE2, INC.
* ALL RIGHTS RESERVED
*
*
* THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
* DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
* IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
* OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
* OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
*
* "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
*
* RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
* ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
* RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
* ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
* IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
* OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
*
*/

#ifndef OBJECTTRACKINGHANDLER_H_
#define OBJECTTRACKINGHANDLER_H_

#include <re2uta/DataHandler.h>
#include <re2uta_inetComms/ObjectTrackingInfo.h>
#include <re2uta_inetComms/ObjectTrackingResponse.h>
#include <geometry_msgs/Pose.h>
#include <visualization_msgs/Marker.h>

namespace re2uta
{

class ObjectTrackingHandler : public DataHandler
{

public:
   ObjectTrackingHandler( ros::NodeHandle node, ReducedMsgController *rmc );
   ~ObjectTrackingHandler();

   void unpack( unsigned char* data );

private:
   ros::Publisher m_RectPub;
   ros::Publisher handle_publish;
   ros::Publisher pipe_publish;
   ros::Subscriber m_uiSub;
   ros::Subscriber m_dataSub;

   void setup();
   void startTracking( const re2uta_inetComms::ObjectTrackingInfo& msg );
   void gotResponse( const re2uta_inetComms::ObjectTrackingResponse& msg);

   void publishHandle(geometry_msgs::Pose& pose);
   void publishPipe(geometry_msgs::Pose& pose);

   void pack( char* params );
   void publish();

   void convertSmallest();
   void convertSmall();
   void convertMedium();
   void convertLarge();
   void convertLargest();
};

} // End namespace

#endif /* OBJECTTRACKINGHANDLER_H_ */
