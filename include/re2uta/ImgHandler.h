/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#ifndef IMGHANDLER_H_
#define IMGHANDLER_H_

#include <re2uta/DataHandler.h>

#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

#include <re2uta_inetComms/ReqImgData.h>
#include <opencv2/imgproc/imgproc.hpp>

namespace re2uta
{

static const int CAM_HEAD   = 1;
static const int CAM_L_HAND = 2;
static const int CAM_R_HAND = 3;

class ImgHandler : public DataHandler
{

public:
    ImgHandler( ros::NodeHandle node, ReducedMsgController *rmc );
    ~ImgHandler();

    void unpack( unsigned char* data );

private:
    image_transport::ImageTransport m_imgTransport;
    image_transport::Subscriber m_imgHeadSub;
    image_transport::Subscriber m_imgLHandSub;
    image_transport::Subscriber m_imgRHandSub;
    image_transport::Publisher m_imgPub;
    cv_bridge::CvImagePtr m_imgPtr;
    cv_bridge::CvImagePtr m_imgHeadPtr;
    cv_bridge::CvImagePtr m_imgLHandPtr;
    cv_bridge::CvImagePtr m_imgRHandPtr;
    ros::Subscriber m_uiSub;
    int m_camera;

    std::string m_rosTopicSubLHandDataVal; // Subscribe to Data messages
    std::string m_rosTopicSubRHandDataVal; // Subscribe to Data messages

    void rcvHeadDataCB( const sensor_msgs::ImageConstPtr& msg );
    void rcvLHandDataCB( const sensor_msgs::ImageConstPtr& msg );
    void rcvRHandDataCB( const sensor_msgs::ImageConstPtr& msg );

    void setup();

    void pack( char* params );
    void publish();
    void requestData( const re2uta_inetComms::ReqImgData& msg );

    void convertSmallest();
    void convertSmall();
    void convertMedium();
    void convertLarge();
    void convertLargest();

    void setCamera(int newCamVal);
};

} // End namespace

#endif /* IMGHANDLER_H_ */
