/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#ifndef LEFTHANDCMDHANDLER_H_
#define LEFTHANDCMDHANDLER_H_

#include <re2uta/DataHandler.h>
#include <osrf_msgs/JointCommands.h>
#include <re2uta_inetComms/JointCommand.h>

namespace re2uta
{

class LeftHandCmdHandler : public DataHandler
{

public:
    LeftHandCmdHandler( ros::NodeHandle node, ReducedMsgController *rmc );
    ~LeftHandCmdHandler();

    void unpack( unsigned char* data );

private:
    ros::Publisher m_cmdPub;
    ros::Subscriber m_cmdSub;

    osrf_msgs::JointCommands m_leftHandCmdsToPublish;
    re2uta_inetComms::JointCommand m_leftHandCmdsToSend;

    void rcvLeftHandCmdDataCB( const re2uta_inetComms::JointCommand& msg );

    void setup();

    void pack( char* params );
    void publish();

    void convertSmallest();
    void convertSmall();
    void convertMedium();
    void convertLarge();
    void convertLargest();
};

} // End namespace

#endif /* LEFTHANDCMDHANDLER_H_ */
