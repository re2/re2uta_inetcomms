/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#ifndef JOINTCMDHANDLER_H_
#define JOINTCMDHANDLER_H_

#include <re2uta/DataHandler.h>
#include <atlas_msgs/AtlasCommand.h>
#include <sensor_msgs/JointState.h>
#include <re2uta_inetComms/JointCommand.h>

namespace re2uta
{

class JointCmdHandler : public DataHandler
{

public:
    JointCmdHandler( ros::NodeHandle node, ReducedMsgController *rmc );
    ~JointCmdHandler();

    void unpack( unsigned char* data );

private:
    bool m_jointStateInitialized;

    ros::Publisher m_cmdPub;
    ros::Subscriber m_cmdSub;
    ros::Subscriber m_jointStateSub;

    atlas_msgs::AtlasCommand m_jointCmdsToPublish;
    re2uta_inetComms::JointCommand m_jointCmdsToSend;

    void rcvJointCmdDataCB( const re2uta_inetComms::JointCommand& msg );
    void rcvJointStateDataCB( const sensor_msgs::JointState& msg );

    void setup();

    void pack( char* params );
    void publish();

    void convertSmallest();
    void convertSmall();
    void convertMedium();
    void convertLarge();
    void convertLargest();
};

} // End namespace

#endif /* JOINTCMDHANDLER_H_ */
