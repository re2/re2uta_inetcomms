/*
 * COPYRIGHT (C) 2005-2013
 * RE2, INC.
 * ALL RIGHTS RESERVED
 *
 *
 * THIS WORK CONTAINS VALUABLE CONFIDENTIAL AND PROPRIETARY INFORMATION.
 * DISCLOSURE OR REPRODUCTION WITHOUT THE WRITTEN AUTHORIZATION OF RE2, INC.
 * IS PROHIBITED. THIS UNPUBLISHED WORK BY RE2, INC. IS PROTECTED BY THE LAWS
 * OF THE UNITED STATES AND OTHER COUNTRIES. IF PUBLICATION OF THE WORK SHOULD
 * OCCUR, THE FOLLOWING NOTICE SHALL APPLY.
 *
 * "COPYRIGHT (C) 2005-2013 RE2, INC. ALL RIGHTS RESERVED."
 *
 * RE2, INC. DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING
 * ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL
 * RE2, INC. BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR
 * ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER
 * IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */

#ifndef LASERROTHANDLER_H_
#define LASERROTHANDLER_H_

#include <re2uta/DataHandler.h>
#include <re2uta/ReducedMsgController.h>
#include <std_msgs/Float64.h>

namespace re2uta
{

class LaserRotHandler : public DataHandler
{

public:
    LaserRotHandler( ros::NodeHandle node, ReducedMsgController *rmc );
    ~LaserRotHandler();

    void unpack( unsigned char* data );

private:
    ros::Timer m_timer;
    ros::Subscriber m_uiSub;
    ros::Publisher  m_rotPub;
    double m_rotSpeed;

    void setup();
    void pack( char* params );
    void publish();

    void convertSmallest();
    void convertSmall();
    void convertMedium();
    void convertLarge();
    void convertLargest();

    bool compareDoubles (double a, double b);
    void requestData( const std_msgs::Float64& msg );
    void initialRotCallback(const ros::TimerEvent&);

};

} // end namespace

#endif /* LASERROTHANDLER_H_ */
